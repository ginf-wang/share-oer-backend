# **SHARE OER** - Backend *(english)*

_(german version below)_

This software was written as part of the Bachelor's Project **BM4OER** at the University of Bremen. Being an early prototype, use in production is strongly discouraged!

Development endet on **31.05.20**.


## Goal

This software provides a simple REST-API to show how an OER-Portal to manage Open Educational Resources could be built.

It consists of data storage of OER, filtered search and a simple user administration including a messenger.


## How to build

The software is built using [Apache Maven](https://maven.apache.org/). To get a working .jar, the goal `clean package` has to be run, resulting in a *jar*-File in the *target* folder.

Alternatively, a Docker container can be built using the provided [Dockerfile](./Dockerfile). Be aware that there is no caching of Maven Dependencies between multiple builds.


## Used Libraries

* [Spring Boot](https://spring.io/projects/spring-boot) licensed under [Apache-2.0](https://github.com/spring-projects/spring-boot/blob/master/LICENSE.txt)
* [H2 Database](https://h2database.com/) licensed under [MPL 2.0](http://www.mozilla.org/MPL/2.0)
* [Hibernate ORM](https://hibernate.org/orm/) licensed under [LGPLv2.1](http://hibernate.org/license/)
* [HikariCP](https://github.com/brettwooldridge/HikariCP) licensed under [Apache-2.0](https://github.com/brettwooldridge/HikariCP/blob/dev/LICENSE)
* [Jackson](http://jackson.codehaus.org/) licensed under [Apache-2.0](https://github.com/FasterXML/jackson-databind/blob/master/LICENSE)
* [Asciidoctor Maven Plugin](https://asciidoctor.org/docs/asciidoctor-maven-plugin/) licensed under [Apache-2.0](https://github.com/asciidoctor/asciidoctor-maven-plugin/blob/master/LICENSE.txt)
* [Git Commit ID Maven Plugin](https://github.com/git-commit-id/git-commit-id-maven-plugin) licensed under [LGPL-3.0](https://github.com/git-commit-id/git-commit-id-maven-plugin/blob/master/LICENSE)


 ## License
 This project is licensed under GNU Affero General Public License v3. See [LICENSE](./LICENSE) for more information.
 

# **SHARE OER** - Backend *(deutsch)*

Diese Software ist im Rahmen des Bachelorprojektes **BM4OER** der Universität Bremen entstanden. Sie stellt einen frühen Prototypen dar und kann keinesfalls produktiv verwendet werden!

Die Entwicklung wurde am **31.05.20** eingestellt.


## Ziel

Diese Software stellt ein einfaches REST-API zur Verfügung, welches aufzeigen soll, wie ein OER-Portal zur Bereitstellung von Freien Lernressourcen aufgebaut sein könnte.

Modelliert ist die Datenhaltung von Lerninhalten, filterbare Suche dieser sowie eine einfache Nutzerverwaltung mit Nachrichtensystem.


## Bauen der Programmdatei

Das Ausführen der Maven Goals `clean package` baut eine ausführbare *jar*-Datei im *target*-Ordner.

Alternativ lassen sich Docker Container auf Basis des vorliegenden [Dockerfiles](./Dockerfile) erstellen. Hierbei ist zu beachten, dass Maven Abhängigkeiten zwischen Bauvorgängen nicht gespeichert werden.


## Lizenz

Dieses Projekt ist lizensiert unter der GNU Affero General Public License v3. Siehe [LICENSE](./LICENSE) für mehr Informationen.
