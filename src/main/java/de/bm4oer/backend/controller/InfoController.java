package de.bm4oer.backend.controller;

import de.bm4oer.backend.dao.InfoDAO;
import de.bm4oer.backend.utils.DatabasePersistor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to gather infos about server state and rebuilding demodata
 *
 * @author jlk@uni-bremen.de
 */
@RestController
@PropertySource("classpath:git.properties")
public class InfoController {

    @Inject
    private InfoDAO infoDAO;
    @Inject
    private DatabasePersistor dbPersistor;
    @Value("${git.build.time}")
    private String buildTime;
    @Value("${git.commit.id.abbrev}")
    private String commitID;
    @Value("${git.commit.user.email}")
    private String commiterMail;

    /**
     * Gathers information about buildTime, last commit and next database reset
     *
     * @return map of server state information
     */
    @GetMapping(value = "info", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> get() {
        final HashMap<String, String> map = new HashMap<>();
        map.put("buildTime", buildTime);
        map.put("commit", commitID + " by " + commiterMail);
        map.put("databaseReset", infoDAO.getRebuildTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return map;
    }

    /**
     * recreates demodata every 30minutes
     */
    @Scheduled(fixedDelay = 1800000)
    public void scheduledTasks() {
        infoDAO.setRebuildTime(LocalDateTime.now().plusMinutes(30));
        dbPersistor.recreateDB();
    }
}
