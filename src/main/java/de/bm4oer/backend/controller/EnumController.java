package de.bm4oer.backend.controller;

import de.bm4oer.backend.model.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * Controller which is responsible for returning all enum types.
 *
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/enums")
@Named
@ApplicationScoped
public class EnumController {

    /**
     * A get request which returns every format type.
     *
     * @return the format types
     */
    @GetMapping("/format")
    public Format[] getFormat() {
        return Format.values();
    }

    /**
     * A get request which returns every license type.
     *
     * @return the license types
     */
    @GetMapping("/license")
    public License[] getLicense() {
        return License.values();
    }

    /**
     * A get request which returns every material type.
     *
     * @return the material types.
     */
    @GetMapping("/materialtype")
    public MaterialType[] getMaterialType() {
        return MaterialType.values();
    }

    /**
     * A get request which returns every subject.
     *
     * @return the subjects.
     */
    @GetMapping("/subject")
    public Subject[] Subject() {
        return Subject.values();
    }

    /**
     * A get request which returns every sort type.
     *
     * @return the sort types.
     */
    @GetMapping("/sort")
    public Sort[] Sort() { return Sort.values(); }
}
