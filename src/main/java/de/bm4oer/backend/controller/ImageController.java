package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.ImageDAO;
import de.bm4oer.backend.exception.DuplicateImageException;
import de.bm4oer.backend.model.Image;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Image Medias.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author hama@uni-bremen.de
 */

@RestController
@RequestMapping("/image")
@Named
@ApplicationScoped
public class ImageController extends MediaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    @Inject
    private ImageDAO imageDAO;

    /**
     * Returns the imagefile of the image belonging to the given id and updates
     * the download counter.
     *
     * @param id the id which belongs to an image.
     * @return the imagefile.
     * @throws IOException        if file not reachable.
     * @throws URISyntaxException if the URI is invalid.
     */
    @Override
    @GetMapping("{id}")
    public ResponseEntity<Resource> get(@PathVariable final UUID id) throws IOException, URISyntaxException {
        imageDAO.incrementDownloads(id);
        return returnResponseEntityFromFile(imageDAO.find(id).getFileName());
    }

    /**
     * Returns every image.
     *
     * @return the images to be returned.
     */
    @GetMapping
    @JsonView(View.Internal.class)
    public List<Image> getAllImage() {
        return imageDAO.getAll();
    }

    /**
     * Creates an image.
     *
     * @param image the image to be created.
     * @return the image which got created.
     */
    @PostMapping
    public Image create(@Valid @RequestBody final Image image) {
        try {
            final Image i = imageDAO.create(image);
            LOGGER.debug("image object with id{} created", image.getId());
            return i;
        } catch (final DuplicateImageException e) {
            LOGGER.error("Could not create image object", e);
        }
        return null;
    }

    /**
     * Updates the image belonging to the given id.
     *
     * @param image the new image replacing the old one.
     * @param id    the id which belongs to an image.
     * @return the updated image.
     */
    @PutMapping("{id}")
    @JsonView(View.Internal.class)
    public Image update(@Valid @RequestBody final Image image, @PathVariable final UUID id) {
        final Image im = imageDAO.update(id, image);
        LOGGER.debug("image object updated", image);
        return im;
    }

    /**
     * Deletes the image belonging to the given id.
     *
     * @param id the id which belongs to an image.
     * @return the image which got deleted.
     */
    @Override
    @DeleteMapping("{id}")
    @JsonView(View.Internal.class)
    public Image delete(@PathVariable final UUID id) {
        final Image image = imageDAO.delete(id);
        LOGGER.debug("image object with id{} deleted", id);
        return image;
    }
}
