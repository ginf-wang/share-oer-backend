package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.MessageDAO;
import de.bm4oer.backend.model.Message;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Messages.
 *
 * @author leowallb@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/message")
@Named
@ApplicationScoped
public class MessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

    @Inject
    private MessageDAO messageDAO;

    /**
     * Creates a message.
     *
     * @param message the message to be created.
     * @return the message which got created.
     */
    @PostMapping
    public Message create(@RequestBody final Message message) {
        final Message m = messageDAO.create(message);
        LOGGER.debug("Message object with owner {} created", message.getId());
        return m;
    }

    /**
     * Returns the message belonging to the given id.
     *
     * @param id the id which belongs to a message.
     * @return the message to be returned.
     */
    @GetMapping(path = "{id}")
    @JsonView(View.Internal.class)
    public Message getByID(@PathVariable final UUID id) {
        final Message message = messageDAO.findByID(id);
        return message;
    }

    /**
     * Returns every message of every user.
     *
     * @return the messages to be returned.
     */
    @GetMapping
    @JsonView(View.Internal.class)
    public List<List<Message>> get() {
        return messageDAO.get();
    }

    /**
     * Deletes the message belonging to the given id.
     *
     * @param id the id which belongs to a message.
     * @return the message which got deleted.
     */
    @DeleteMapping(path = "{id}")
    public Message delete(@PathVariable final UUID id) {
        final Message message = messageDAO.delete(id);
        LOGGER.debug("message object with id {} deleted", id);
        return message;
    }

    /**
     * Updates the message belonging to the given id.
     *
     * @param id      the id which belongs to a message.
     * @param message the new message replacing the old one.
     * @return the updated message.
     */
    @PutMapping(path = "{id}")
    public Message update(@PathVariable final UUID id, @RequestBody final Message message) {
        final Message m = messageDAO.update(id, message);
        LOGGER.debug("message object with id {} updated", message.getId());
        return m;
    }
}
