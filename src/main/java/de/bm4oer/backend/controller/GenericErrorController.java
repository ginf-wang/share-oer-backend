package de.bm4oer.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Handler of all uncatched errors
 *
 * @author jlk@uni-bremen.de
 */
@RestController
@RequestMapping("/error")
public class GenericErrorController implements ErrorController {

    private final ErrorAttributes errorAttributes;

    @Autowired
    public GenericErrorController(final ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    /**
     * Returns the error path.
     *
     * @return the error path.
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }

    /**
     * returns error information as JSON
     *
     * @param aRequest faulty request
     * @return JSON body with error trace, code and message
     */
    @ResponseBody
    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> error(final HttpServletRequest aRequest) {
        final Map<String, Object> body = getErrorAttributes(aRequest, getTraceParameter(aRequest));
        final String trace = (String) body.get("trace");
        if (trace != null) {
            final String[] lines = trace.split("\n\t");
            body.put("trace", lines);
        }
        return body;
    }

    /**
     * evaluates if trace of request is given
     *
     * @param request to evaluate
     * @return true if trace given and is true
     */
    private boolean getTraceParameter(final HttpServletRequest request) {
        final String parameter = request.getParameter("trace");
        if (parameter == null) {
            return false;
        }
        return !"false".equalsIgnoreCase(parameter);
    }

    /**
     * returns error attributes of faulty request
     *
     * @param request           the faulty request to evaluate
     * @param includeStackTrace if stracktrace should be included
     * @return
     */
    private Map<String, Object> getErrorAttributes(final HttpServletRequest request, final boolean includeStackTrace) {
        final WebRequest webRequest = new ServletWebRequest(request);
        return errorAttributes.getErrorAttributes(webRequest, includeStackTrace);
    }
}
