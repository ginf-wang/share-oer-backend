package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.UserDAO;
import de.bm4oer.backend.exception.DuplicateUserException;
import de.bm4oer.backend.model.User;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Users.
 *
 * @author hama@uni-bremen.de
 * @author jlk@uni-bremen.de
 */
@RestController
@RequestMapping("/user")
@Named
@ApplicationScoped
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Inject
    private UserDAO userDAO;

    /**
     * Creates a User.
     *
     * @param user the user to be created.
     * @return the user which got created.
     */
    @PostMapping
    public User create(@RequestBody final User user) {
        try {
            final User u = userDAO.create(user);
            LOGGER.debug("User object with email {} created", user.getEmail());
            return u;
        } catch (final DuplicateUserException e) {
            LOGGER.error("Could not create user object, {}", e.getMessage());
            return null;
        }
    }

    @GetMapping(path = "{email}")
    @JsonView(View.InternalUser.class)
    public User getByEmail(@PathVariable final String email) {
        final User user = userDAO.findByEmail(email);
        return user;
    }

    /**
     * Returns every user.
     *
     * @return the users to be returned.
     */
    @GetMapping
    @JsonView(View.Public.class)
    public List<User> get() {
        return userDAO.get();
    }

    /**
     * Deletes the user belonging to the given email.
     *
     * @param email the email which belongs to a user.
     * @return the user which got deleted.
     */
    @DeleteMapping(path = "{email}")
    @JsonView(View.InternalUser.class)
    public User delete(@PathVariable final String email) {
        final User u = userDAO.delete(email);
        LOGGER.debug("User object with email {} deleted", email);
        return u;
    }

    /**
     * Updates the user belonging to the given email.
     *
     * @param email the email which belongs to a user.
     * @param user  the new user replacing the old one.
     * @return the updated user.
     */
    @PutMapping(path = "{email}")
    @JsonView(View.InternalUser.class)
    public User update(@PathVariable final String email, @RequestBody final User user) {
        final User u = userDAO.update(email, user);
        if (u != null) {
            LOGGER.debug("User object with email {} updated", user.getEmail());
            return u;
        } else {
            LOGGER.error("Could not update user {}", user.getUsername());
            return null;
        }
    }
}
