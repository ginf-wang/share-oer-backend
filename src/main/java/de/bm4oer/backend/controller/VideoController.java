package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.VideoDAO;
import de.bm4oer.backend.exception.DuplicateVideoException;
import de.bm4oer.backend.model.Video;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Video Medias.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/video")
@Named
@ApplicationScoped
public class VideoController extends MediaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoController.class);

    @Inject
    private VideoDAO videoDAO;

    /**
     * Returns the videofile of the video belonging to the given id and updates
     * the download counter.
     *
     * @param id the id which belongs to a video.
     * @return the videofile.
     * @throws IOException        if file not reachable.
     * @throws URISyntaxException if the URI is invalid.
     */
    @Override
    @GetMapping("{id}")
    public ResponseEntity<Resource> get(@PathVariable final UUID id) throws IOException, URISyntaxException {
        videoDAO.incrementDownloads(id);
        return returnResponseEntityFromFile(videoDAO.find(id).getFileName());
    }

    /**
     * Returns every Video.
     *
     * @return the videos to be returned.
     */
    @GetMapping
    @JsonView(View.Internal.class)
    public List<Video> getAllVideo() {
        return videoDAO.getAll();
    }

    /**
     * Creates a video.
     *
     * @param video the video to be created.
     * @return the video which got created.
     */
    @PostMapping
    public Video create(@Valid @RequestBody final Video video) {
        try {
            final Video vi = videoDAO.create(video);
            LOGGER.debug("video object with id{} created", video.getId());
            return vi;
        } catch (final DuplicateVideoException e) {
            LOGGER.error("Could not create video object", e);
        }
        return null;
    }

    /**
     * Updates the video belonging to the given id.
     *
     * @param video the new video replacing the old one.
     * @param id    the id which belongs to a video.
     * @return the updated video.
     */
    @PutMapping("{id}")
    @JsonView(View.Internal.class)
    public Video update(@Valid @RequestBody final Video video, @PathVariable final UUID id) {
        final Video vi = videoDAO.update(id, video);
        LOGGER.debug("video object updated", video);
        return vi;
    }

    /**
     * Deletes the video belonging to the given id.
     *
     * @param id the id which belongs to a video.
     * @return the video which got deleted.
     */
    @Override
    @DeleteMapping("{id}")
    @JsonView(View.Internal.class)
    public Video delete(@PathVariable final UUID id) {
        final Video video = videoDAO.delete(id);
        LOGGER.debug("video object with id{} deleted", id);
        return video;
    }
}
