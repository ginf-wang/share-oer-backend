package de.bm4oer.backend.controller;

import de.bm4oer.backend.dao.FilterDAO;
import de.bm4oer.backend.exception.DuplicateFilterException;
import de.bm4oer.backend.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating and
 * deleting all filter types.
 *
 * @author decker1@uni-bremen.de
 */
@RestController
@RequestMapping("/filter")
@Named
@ApplicationScoped
public class FilterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FilterController.class);

    @Inject
    private FilterDAO filterDAO;

    /**
     * Returns the datefilter belonging to the given id.
     *
     * @param id the id which belongs to a datefilter.
     * @return the datefilter to be returned.
     */
    @GetMapping("date/{id}")
    public DateFilter getDateFilter(@PathVariable final String id) {
        final DateFilter dfilter = filterDAO.findDateFilter(UUID.fromString(id));
        return dfilter;
    }

    /**
     * Creates a datefilter.
     *
     * @param dateFilter the datefilter to be created.
     * @return the datefilter which got created.
     */
    @PostMapping("date")
    public DateFilter createDateFilter(@Valid @RequestBody final DateFilter dateFilter) {
        try {
            final DateFilter dFilter = filterDAO.createDateFilter(dateFilter);
            LOGGER.debug("datefilter object with id {} created", dateFilter.getId());
            return dFilter;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create datefilter object", e);
        }
        return null;
    }

    /**
     * Deletes the datefilter belonging to the given id.
     *
     * @param id the id which belongs to a datefilter.
     * @return the datefilter which got deleted.
     */
    @DeleteMapping("date/{id}")
    public DateFilter deleteDateFilter(@PathVariable final String id) {
        final DateFilter dFilter = filterDAO.deleteDateFilter(filterDAO.findDateFilter(UUID.fromString(id)));
        LOGGER.debug("datefilter object with id{} deleted", id);
        return dFilter;
    }

    /**
     * Returns the classfilter belonging to the given id.
     *
     * @param id the id which belongs to a classfilter.
     * @return the classfilter to be returned.
     */
    @GetMapping("class/{id}")
    public ClassFilter getClassFilter(@PathVariable final String id) {
        final ClassFilter cFilter = filterDAO.findClassFilter(UUID.fromString(id));
        return cFilter;
    }

    /**
     * Creates a classfilter.
     *
     * @param classFilter the classfilter to be created.
     * @return the classfilter which got created.
     */
    @PostMapping("class")
    public ClassFilter createClassFilter(@Valid @RequestBody final ClassFilter classFilter) {
        try {
            final ClassFilter cFilter = filterDAO.createClassFilter(classFilter);
            LOGGER.debug("classfilter object with id {} created", classFilter.getId());
            return cFilter;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create classfilter object", e);
        }
        return null;
    }

    /**
     * Deletes the classfilter belonging to the given id.
     *
     * @param id the id which belongs to a classfilter.
     * @return the classfilter which got deleted.
     */
    @DeleteMapping("class/{id}")
    public ClassFilter deleteClassFilter(@PathVariable final String id) {
        final ClassFilter cFilter = filterDAO.deleteClassFilter(filterDAO.findClassFilter(UUID.fromString(id)));
        LOGGER.debug("classfilter object with id{} deleted", id);
        return cFilter;
    }

    /**
     * Returns the materialtypefilter belonging to the given id.
     *
     * @param id the id which belongs to a materialtypefilter.
     * @return the materialtypefilter to be returned.
     */
    @GetMapping("materialtype/{id}")
    public MaterialTypeFilter getMaterialTypeFilter(@PathVariable final String id) {
        final MaterialTypeFilter mFilter = filterDAO.findMaterialTypeFilter(UUID.fromString(id));
        return mFilter;
    }

    /**
     * Creates a materialtypefilter.
     *
     * @param materialTypeFilter the materialtypefilter to be created.
     * @return the materialtypefilter which got created.
     */
    @PostMapping("materialtype")
    public MaterialTypeFilter createMaterialTypeFilter(@Valid @RequestBody final MaterialTypeFilter materialTypeFilter) {
        try {
            final MaterialTypeFilter mFilter = filterDAO.createMaterialTypeFilter(materialTypeFilter);
            LOGGER.debug("materialtypefilter object with id {} created", materialTypeFilter.getId());
            return mFilter;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create materialtypefilter object", e);
        }
        return null;
    }

    /**
     * Deletes the materialtypefilter belonging to the given id.
     *
     * @param id the id which belongs to a materialtypefilter.
     * @return the materialtypefilter which got deleted.
     */
    @DeleteMapping("materialtype/{id}")
    public MaterialTypeFilter deleteMaterialTypeFilter(@PathVariable final String id) {
        final MaterialTypeFilter mFilter =
                filterDAO.deleteMaterialTypeFilter(filterDAO.findMaterialTypeFilter(UUID.fromString(id)));
        LOGGER.debug("materialtypefilter object with id{} deleted", id);
        return mFilter;
    }

    /**
     * Returns the subjectfilter belonging to the given id.
     *
     * @param id the id which belongs to a subjectfilter.
     * @return the datefilter to be returned.
     */
    @GetMapping("subject/{id}")
    public SubjectFilter getSubjectFilter(@PathVariable final String id) {
        final SubjectFilter subjectFilter = filterDAO.findSubjectFilter(UUID.fromString(id));
        return subjectFilter;
    }

    /**
     * Creates a subjectfilter.
     *
     * @param subjectFilter the subjectfilter to be created.
     * @return the subjectfilter which got created.
     */
    @PostMapping("subject")
    public SubjectFilter createSubjectFilter(@Valid @RequestBody final SubjectFilter subjectFilter) {
        try {
            final SubjectFilter sFilter = filterDAO.createSubjectFilter(subjectFilter);
            LOGGER.debug("subjectfilter object with id {} created", subjectFilter.getId());
            return sFilter;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create subjectfilter object", e);
        }
        return null;
    }

    /**
     * Deletes the subjectfilter belonging to the given id.
     *
     * @param id the id which belongs to a subjectfilter.
     * @return the subjectfilter which got deleted.
     */
    @DeleteMapping("subject/{id}")
    public SubjectFilter deleteSubjectFilter(@PathVariable final String id) {
        final SubjectFilter sFilter = filterDAO.deleteSubjectFilter(filterDAO.findSubjectFilter(UUID.fromString(id)));
        LOGGER.debug("subjectfilter object with id{} deleted", id);
        return sFilter;
    }

    /**
     * Returns the formatfilter belonging to the given id.
     *
     * @param id the id which belongs to a formatfilter.
     * @return the formatfilter to be returned.
     */
    @GetMapping("format/{id}")
    public FormatFilter getFormatFilter(@PathVariable final String id) {
        final FormatFilter formatFilter = filterDAO.findFormatFilter(UUID.fromString(id));
        return formatFilter;
    }

    /**
     * Creates a formatfilter.
     *
     * @param formatFilter the formatfilter to be created.
     * @return the formatfilter which got created.
     */
    @PostMapping("format")
    public FormatFilter createFormatFilter(@Valid @RequestBody final FormatFilter formatFilter) {
        try {
            final FormatFilter fFilter = filterDAO.createFormatFilter(formatFilter);
            LOGGER.debug("formatfilter object with id {} created", formatFilter.getId());
            return fFilter;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create formatfilter object", e);
        }
        return null;
    }

    /**
     * Deletes the formatfilter belonging to the given id.
     *
     * @param id the id which belongs to a formatfilter.
     * @return the formatfilter which got deleted.
     */
    @DeleteMapping("format/{id}")
    public FormatFilter deleteFormatFilter(@PathVariable final String id) {
        final FormatFilter fFilter = filterDAO.deleteFormatFilter(filterDAO.findFormatFilter(UUID.fromString(id)));
        LOGGER.debug("formatfilter object with id{} deleted", id);
        return fFilter;
    }

    /**
     * Returns the idfilter belonging to the given id.
     *
     * @param id the id which belongs to a idfilter.
     * @return the idfilter to be returned.
     */
    @GetMapping("id/{id}")
    public IdFilter getIdFilter(@PathVariable final String id) {
        final IdFilter idFilter = filterDAO.findIdFilter(UUID.fromString(id));
        return idFilter;
    }

    /**
     * Creates a idfilter.
     *
     * @param idFilter the idfilter to be created.
     * @return the idfilter which got created.
     */
    @PostMapping("id")
    public IdFilter createIdFilter(@Valid @RequestBody final IdFilter idFilter) {
        try {
            final IdFilter idf = filterDAO.createIdFilter(idFilter);
            LOGGER.debug("idfilter object with id {} created", idFilter.getId());
            return idf;
        } catch (final DuplicateFilterException e) {
            LOGGER.error("Could not create idfilter object", e);
        }
        return null;
    }

    /**
     * Deletes the idfilter belonging to the given id.
     *
     * @param id the id which belongs to a idfilter.
     * @return the idfilter which got deleted.
     */
    @DeleteMapping("id/{id}")
    public IdFilter deleteIdFilter(@PathVariable final String id) {
        final IdFilter idf = filterDAO.deleteIdFilter(filterDAO.findIdFilter(UUID.fromString(id)));
        LOGGER.debug("idfilter object with id{} deleted", id);
        return idf;
    }
}
