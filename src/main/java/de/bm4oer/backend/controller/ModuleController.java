package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.ModuleDAO;
import de.bm4oer.backend.exception.DuplicateModuleException;
import de.bm4oer.backend.model.Module;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Modules.
 *
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/module")
@Named
@ApplicationScoped
public class ModuleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleController.class);

    @Inject
    private ModuleDAO moduleDAO;

    /**
     * Creates a module.
     *
     * @param module the module to be created.
     * @return the module which got created.
     */
    @PostMapping
    @JsonView(View.Public.class)
    public Module create(@RequestBody final Module module) {
        try {
            final Module m = moduleDAO.create(module);
            LOGGER.debug("module object with id {} created", module.getId());
            return m;
        } catch (final DuplicateModuleException e) {
            LOGGER.error("Could not create module object", e);
        }
        return null;
    }

    /**
     * Returns the module belonging to the given id.
     *
     * @param id the id which belongs to a module.
     * @return the module to be returned.
     */
    @GetMapping(path = "{id}")
    @JsonView(View.Public.class)
    public Module getByID(@PathVariable final UUID id) {
        final Module mol = moduleDAO.findByID(id);
        return mol;
    }

    /**
     * Returns every module.
     *
     * @return the modules to be returned.
     */
    @GetMapping
    @JsonView(View.Public.class)
    public List<Module> get() {
        return moduleDAO.get();
    }

    /**
     * Deletes the module belonging to the given id.
     *
     * @param id the id which belongs to a module.
     * @return the module which got deleted.
     */
    @DeleteMapping(path = "{id}")
    @JsonView(View.Public.class)
    public Module delete(@PathVariable final UUID id) {
        final Module m = moduleDAO.delete(id);
        LOGGER.debug("module object with id {} deleted", id);
        return m;
    }

    /**
     * Updates the module belonging to the given id.
     *
     * @param id     the id which belongs to a module.
     * @param module the new module replacing the old one.
     * @return the updated module.
     */
    @PutMapping(path = "{id}")
    @JsonView(View.Public.class)
    public Module update(@PathVariable final UUID id, @RequestBody final Module module) {
        final Module m = moduleDAO.update(id, module);
        LOGGER.debug("module object with id {} updated", module.getId());
        return m;
    }
}
