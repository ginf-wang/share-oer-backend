package de.bm4oer.backend.controller;

import de.bm4oer.backend.dao.SearchDAO;
import de.bm4oer.backend.exception.DuplicateSearchException;
import de.bm4oer.backend.model.Filter;
import de.bm4oer.backend.model.Search;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating and
 * deleting Searches.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/search")
@Named
@ApplicationScoped
public class SearchController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    @Inject
    private SearchDAO searchDAO;

    /**
     * Returns the search belonging to the given id.
     *
     * @param id the id which belongs to a seach.
     * @return the search to be returned.
     */
    @GetMapping("{id}")
    public Search get(@PathVariable final String id) {
        return searchDAO.find(UUID.fromString(id));
    }

    /**
     * Creates a search.
     *
     * @param search the search to be created.
     * @return the search which got created.
     */
    @PostMapping
    public Search create(@RequestBody final Search search) {
        if (search.getFilters() != null) {
            for (final Filter filter : search.getFilters()) {
                filter.setId(UUID.randomUUID());
            }
        }
        if (search.getId() == null) {
            search.setId(UUID.randomUUID());
        }
        try {
            final Search s = searchDAO.create(search);
            LOGGER.debug("search object with id{} created", search.getId());
            return s;
        } catch (final DuplicateSearchException e) {
            LOGGER.error("Could not create search object", e);
            return null;
        }
    }

    /**
     * Returns every search.
     *
     * @return the seaches to be returned.
     */
    @GetMapping
    public List<Search> getAll() {
        return searchDAO.getAllSearches();
    }

    /**
     * Deletes the search belonging to the given id.
     *
     * @param id the id which belongs to a search.
     * @return the search which got deleted.
     */
    @DeleteMapping("{id}")
    public Search delete(@PathVariable final UUID id) {
        final Search search = searchDAO.delete(id);
        if (Objects.nonNull(search)) {
            LOGGER.debug("search object with id{} deleted", id);
        } else {
            LOGGER.debug("search object with id{} has not been deleted", id);
        }
        return search;
    }
}
