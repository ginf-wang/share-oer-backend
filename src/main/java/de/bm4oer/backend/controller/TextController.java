package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.TextDAO;
import de.bm4oer.backend.exception.DuplicateTextException;
import de.bm4oer.backend.model.Text;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

/**
 * Controller which is responsible for returning, creating, updating and
 * deleting Text Medias.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@RestController
@RequestMapping("/text")
@Named
@ApplicationScoped
public class TextController extends MediaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextController.class);

    @Inject
    private TextDAO textDAO;

    /**
     * Returns the textfile of the text belonging to the given id and updates
     * the download counter.
     *
     * @param id the id which belongs to a text.
     * @return the textfile.
     * @throws IOException        if file not reachable.
     * @throws URISyntaxException if the URI is invalid.
     */
    @Override
    @GetMapping("{id}")
    public ResponseEntity<Resource> get(@PathVariable final UUID id) throws IOException, URISyntaxException {
        textDAO.incrementDownloads(id);
        return returnResponseEntityFromFile(textDAO.find(id).getFileName());
    }

    /**
     * Returns every Text.
     *
     * @return the texts to be returned.
     */
    @GetMapping
    @JsonView(View.Internal.class)
    public List<Text> getAllText() {
        return textDAO.getAll();
    }

    /**
     * Creates a text.
     *
     * @param text the text to be created.
     * @return the text which got created.
     */
    @PostMapping
    public Text create(@Valid @RequestBody final Text text) {
        try {
            final Text t = textDAO.create(text);
            LOGGER.debug("text object with id{} created", text.getId());
            return t;
        } catch (final DuplicateTextException e) {
            LOGGER.error("Could not create text object", e);
        }
        return null;
    }

    /**
     * Updates the text belonging to the given id.
     *
     * @param text the new text replacing the old one.
     * @param id   the id which belongs to a text.
     * @return the updated text.
     */
    @PutMapping("{id}")
    @JsonView(View.Internal.class)
    public Text update(@Valid @RequestBody final Text text, @PathVariable final UUID id) {
        final Text te = textDAO.update(id, text);
        LOGGER.debug("text object updated", text);
        return te;
    }

    /**
     * Deletes the text belonging to the given id.
     *
     * @param id the id which belongs to a text.
     * @return the text which got deleted.
     */
    @Override
    @DeleteMapping("{id}")
    @JsonView(View.Internal.class)
    public Text delete(@PathVariable final UUID id) {
        final Text text = textDAO.delete(id);
        LOGGER.debug("text object with id{} deleted", id);
        return text;
    }
}
