package de.bm4oer.backend.controller;

import com.fasterxml.jackson.annotation.JsonView;
import de.bm4oer.backend.dao.MediaDAO;
import de.bm4oer.backend.model.Media;
import de.bm4oer.backend.model.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Controller which is responsible for returning and deleting any Media type.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author hama@uni-bremen.de
 */

@RestController
@Named
@ApplicationScoped
public class MediaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MediaController.class);
    private final String mediaPath = System.getProperty("user.dir") + "/src/test/resources/media/";
    @Inject
    private MediaDAO mediaDAO;

    /**
     * Returns the file of the media belonging to the given id and updates
     * the download counter.
     *
     * @param id the id which belongs to a media.
     * @return the file.
     * @throws IOException        if file not reachable.
     * @throws URISyntaxException if the URI is invalid.
     */
    @GetMapping("/media/{id}")
    public ResponseEntity<Resource> get(@PathVariable final UUID id) throws IOException, URISyntaxException {
        mediaDAO.incrementDownloads(id);
        return returnResponseEntityFromFile(mediaDAO.find(id).getFileName());
    }

    /**
     * Returns every media by a given search.
     *
     * @param id       the id of the first media to be returned.
     * @param results  the maximum number of results which will be returned.
     * @param searchId the id of the search.
     * @return the medias to be returned.
     */
    @GetMapping("/media")
    @JsonView(View.Internal.class)
    public List<Media> getAllMedia(@RequestParam(required = false) final UUID id,
                                   @RequestParam(required = false, defaultValue = "10") final int results,
                                   @RequestParam(required = false) final UUID searchId) {
        return mediaDAO.findAll(id, results, searchId);
    }

    /**
     * Deletes the media and the file of the media beloging to the given id.
     *
     * @param id the id which belongs to a media.
     * @return the deleted file.
     */
    @DeleteMapping("/media/{id}")
    @JsonView(View.Internal.class)
    public Media delete(@PathVariable final UUID id) {
        final Media media = mediaDAO.delete(id);
        LOGGER.debug("image object with id{} deleted", id);
        return media;
    }

    ResponseEntity<Resource> returnResponseEntityFromFile(final String fileName)
            throws IOException, URISyntaxException {
        final Pattern p = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
        final Matcher m = p.matcher(fileName);

        if (!m.find()) {
            final FileInputStream fileInputStream = new FileInputStream(mediaPath + fileName);
            final InputStreamResource resource = new InputStreamResource(fileInputStream);
            final ContentDisposition contentDisposition = ContentDisposition.builder("inline")
                                                                            .filename(fileName)
                                                                            .build();
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentDisposition(contentDisposition);

            final String mimeType = URLConnection.guessContentTypeFromName(fileName);

            return ResponseEntity.ok()
                                 .contentType(MediaType.parseMediaType(mimeType))
                                 .headers(headers)
                                 .body(resource);
        } else {
            final URI u = new URI(fileName);
            final HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setLocation(u);
            return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
        }
    }
}
