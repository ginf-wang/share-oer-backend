package de.bm4oer.backend.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

/**
 * Redirects root URI to API doc
 *
 * @author jlk@uni-bremen.de
 */
@RestController
@RequestMapping("/")
@Named
@ApplicationScoped
public class RootController {

    /**
     * @return Redirect to URI of API doc
     */
    @GetMapping
    public RedirectView redirectToDocs() {
        return new RedirectView("api-doc.html");
    }
}
