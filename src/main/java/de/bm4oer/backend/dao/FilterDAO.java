package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateFilterException;
import de.bm4oer.backend.model.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.UUID;

/**
 * DAO for all subclasses of filter
 *
 * @author decker1@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class FilterDAO {
    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the filter to the database and returns it.
     *
     * @param dateFilter the filter to be added
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public DateFilter createDateFilter(final DateFilter dateFilter) throws DuplicateFilterException {
        if (dateFilter.getId() == null) {
            dateFilter.setId(UUID.randomUUID());
        }
        if (dateFilter != null) {
            synchronized (DateFilter.class) {
                if (findDateFilter(dateFilter.getId()) == null) {
                    em.persist(dateFilter);
                    return dateFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public DateFilter findDateFilter(final UUID id) {
        final DateFilter datef = em.find(DateFilter.class, id);
        return datef;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param dateFilter the filter
     * @return the removed filter
     */
    public DateFilter deleteDateFilter(final DateFilter dateFilter) {
        if (em.contains(dateFilter)) {
            em.remove(dateFilter);
            return dateFilter;
        } else {
            return null;
        }
    }

    /**
     * Adds the filter to the database and returns it.
     *
     * @param classFilter the filter to be added
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public ClassFilter createClassFilter(final ClassFilter classFilter) throws DuplicateFilterException {
        if (classFilter.getId() == null) {
            classFilter.setId(UUID.randomUUID());
        }
        if (classFilter != null) {
            synchronized (ClassFilter.class) {
                if (findClassFilter(classFilter.getId()) == null) {
                    em.persist(classFilter);
                    return classFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public ClassFilter findClassFilter(final UUID id) {
        final ClassFilter classf = em.find(ClassFilter.class, id);
        return classf;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param classFilter the filter
     * @return the removed filter
     */
    public ClassFilter deleteClassFilter(final ClassFilter classFilter) {
        if (em.contains(classFilter)) {
            em.remove(classFilter);
            return classFilter;
        }
        return null;
    }

    /**
     * Adds the filter to the database and returns it.
     *
     * @param materialTypeFilter the filter to be added.
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public MaterialTypeFilter createMaterialTypeFilter(final MaterialTypeFilter materialTypeFilter)
            throws DuplicateFilterException {
        if (materialTypeFilter.getId() == null) {
            materialTypeFilter.setId(UUID.randomUUID());
        }
        if (materialTypeFilter != null) {
            synchronized (MaterialTypeFilter.class) {
                if (findMaterialTypeFilter(materialTypeFilter.getId()) == null) {
                    em.persist(materialTypeFilter);
                    return materialTypeFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public MaterialTypeFilter findMaterialTypeFilter(final UUID id) {
        final MaterialTypeFilter matef = em.find(MaterialTypeFilter.class, id);
        return matef;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param materialTypeFilter the filter
     * @return the removed filter
     */
    public MaterialTypeFilter deleteMaterialTypeFilter(final MaterialTypeFilter materialTypeFilter) {
        if (em.contains(materialTypeFilter)) {
            em.remove(materialTypeFilter);
            return materialTypeFilter;
        }
        return null;
    }

    /**
     * Adds the filter to the database and returns it.
     *
     * @param formatFilter the filter to be added
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public FormatFilter createFormatFilter(final FormatFilter formatFilter) throws DuplicateFilterException {
        if (formatFilter.getId() == null) {
            formatFilter.setId(UUID.randomUUID());
        }
        if (formatFilter != null) {
            synchronized (FormatFilter.class) {
                if (findFormatFilter(formatFilter.getId()) == null) {
                    em.persist(formatFilter);
                    return formatFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public FormatFilter findFormatFilter(final UUID id) {
        final FormatFilter forf = em.find(FormatFilter.class, id);
        return forf;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param formatFilter the filter
     * @return the removed filter
     */
    public FormatFilter deleteFormatFilter(final FormatFilter formatFilter) {
        if (em.contains(formatFilter)) {
            em.remove(formatFilter);
            return formatFilter;
        }
        return null;
    }

    /**
     * Adds the filter to the database and returns it.
     *
     * @param subjectFilter the filter to be added.
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public SubjectFilter createSubjectFilter(final SubjectFilter subjectFilter) throws DuplicateFilterException {
        if (subjectFilter.getId() == null) {
            subjectFilter.setId(UUID.randomUUID());
        }
        if (subjectFilter != null) {
            synchronized (SubjectFilter.class) {
                if (findSubjectFilter(subjectFilter.getId()) == null) {
                    em.persist(subjectFilter);
                    return subjectFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public SubjectFilter findSubjectFilter(final UUID id) {
        final SubjectFilter subf = em.find(SubjectFilter.class, id);
        return subf;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param subjectFilter the filter
     * @return the removed filter
     */
    public SubjectFilter deleteSubjectFilter(final SubjectFilter subjectFilter) {
        if (em.contains(subjectFilter)) {
            em.remove(subjectFilter);
            return subjectFilter;
        } else {
            return null;
        }
    }

    /**
     * Adds the filter to the database and returns it.
     *
     * @param idFilter the filter to be added.
     * @return the added filter
     * @throws DuplicateFilterException if the id generation fails and a duplicate id is created
     */
    public IdFilter createIdFilter(final IdFilter idFilter) throws DuplicateFilterException {
        if (idFilter.getId() == null) {
            idFilter.setId(UUID.randomUUID());
        }
        if (idFilter != null) {
            synchronized (IdFilter.class) {
                if (findIdFilter(idFilter.getId()) == null) {
                    em.persist(idFilter);
                    return idFilter;
                } else {
                    final DuplicateFilterException duplicateFilterException = new DuplicateFilterException();
                    throw duplicateFilterException;
                }
            }
        }
        return null;
    }

    /**
     * Finds the filter with the id specified in id and returns it.
     *
     * @param id the id of the filter to be found
     * @return the filter or null if no filter with the id exists
     */
    public IdFilter findIdFilter(final UUID id) {
        final IdFilter idf = em.find(IdFilter.class, id);
        return idf;
    }

    /**
     * Removes the filter from the database and returns it.
     *
     * @param idFilter the filter
     * @return the removed filter
     */
    public IdFilter deleteIdFilter(final IdFilter idFilter) {
        if (em.contains(idFilter)) {
            em.remove(idFilter);
            return idFilter;
        } else {
            return null;
        }
    }
}
