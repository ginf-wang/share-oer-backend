package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateUserException;
import de.bm4oer.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * DAO for objects of search class.
 *
 * @author hama@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class UserDAO {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the user to the db and returns it.
     *
     * @param user the user to be added
     * @return the added user
     * @throws DuplicateUserException if an user with the email already exists
     */
    public User create(final User user) throws DuplicateUserException {
        if (user != null) {
            synchronized (User.class) {
                if (findByEmail(user.getEmail()) == null) {
                    user.setDateCreated(java.time.LocalDateTime.now());
                    em.persist(user);
                    return user;
                } else {
                    throw new DuplicateUserException();
                }
            }
        }
        return null;
    }

    /**
     * Finds and returns the user with the email specified in email.
     *
     * @param email email of the user to be found
     * @return the found user or null if no such user is found
     */
    public User findByEmail(final String email) {
        if (email == null) {
            return null;
        } else {
            return em.find(User.class, email);
        }
    }

    /**
     * Updates the user with the email specified in email with the data of user and returns the updated user.
     *
     * @param email email of the user to be updated
     * @param user  user object that contains new data
     * @return the updated user or null if email or user are null
     */
    public User update(final String email, final User user) {
        if (user != null && email != null) {
            final User userToUpdate = findByEmail(email);
            if (userToUpdate != null) {
                synchronized (User.class) {
                    user.setDateCreated(userToUpdate.getDateCreated());
                    em.remove(userToUpdate);
                    em.persist(user);
                    return user;
                }
            }
        }
        return null;
    }

    public User delete(final String email) {
        final User user = findByEmail(email);
        em.remove(user);
        return user;
    }

    /**
     * Finds and returns all users.
     *
     * @return a list of all users
     */
    public List<User> get() {
        return em.createQuery("Select u From User u").getResultList();
    }
}
