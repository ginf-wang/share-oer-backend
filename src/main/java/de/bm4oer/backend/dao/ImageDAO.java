package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateImageException;
import de.bm4oer.backend.model.Image;
import de.bm4oer.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * DAO for objects of image class.
 *
 * @author decker1@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class ImageDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the image to the db and returns it.
     *
     * @param image the image to be added
     * @return the added image
     * @throws DuplicateImageException if the id generation fails and a duplicate id is created
     */
    public Image create(final Image image) throws DuplicateImageException {
        final LocalDateTime time = java.time.LocalDateTime.now();
        if (image.getId() == null) {
            image.setId(UUID.randomUUID());
            image.setDateCreated(time);
        }
        if (find(image.getId()) == null) {
            image.setDateUpdated(time);
            em.persist(image);
        } else {
            throw new DuplicateImageException();
        }
        return image;
    }

    /**
     * Finds and returns the image with the id specified in id.
     *
     * @param id id of the image to be found
     * @return the found image or null if no such image is found
     */
    public Image find(final UUID id) {
        final Image image = em.find(Image.class, id);
        return image;
    }

    /**
     * Updates the image with the id specified in id with the data of image and returns the updated image.
     *
     * @param id    id of the image to be updated
     * @param image object that contains new data
     * @return the updated image
     */
    public Image update(final UUID id, final Image image) {
        final Image oldimage = find(id);
        if (image != null) {
            synchronized (Image.class) {
                image.setId(oldimage.getId());
                image.setDateCreated(oldimage.getDateCreated());
                image.setDateUpdated(java.time.LocalDateTime.now());
                em.merge(image);
                return image;
            }
        }
        return null;
    }

    /**
     * Deletes and returns the image with the id specified in id.
     *
     * @param id id of the image to be deleted
     * @return the deleted image
     */
    public Image delete(final UUID id) {
        final Image image = em.find(Image.class, id);
        if (image != null) {
            synchronized (Image.class) {
                removeFavorites(image);
                image.getOwner().getMyMedia().remove(image);
            }
            return image;
        }
        return null;
    }

    /**
     * Finds and returns all images.
     *
     * @return the found images.
     */
    public List<Image> getAll() {
        return em.createQuery("Select n from Image n").getResultList();
    }

    /**
     * Increases the download counter of the image with the id by one.
     *
     * @param id id of the image that has been downloaded
     */
    public void incrementDownloads(final UUID id) {
        final Image i = find(id);
        final int incrementedDownloads = i.getDownloads() + 1;
        i.setDownloads(incrementedDownloads);
        em.persist(i);
    }

    /**
     * Removes the image from the favorite lists of all users.
     *
     * @param image the image that is removed
     */
    private void removeFavorites(final Image image) {
        final List<User> list = em.createQuery("Select u from User u").getResultList();
        for (final User u : list) {
            u.getFavourites().remove(image);
        }
    }
}
