package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateModuleException;
import de.bm4oer.backend.model.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.annotation.RequestScope;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * DAO for objects of module class.
 *
 * @author hama@uni-bremen.de
 */
@Transactional
@Named
@RequestScope
public class ModuleDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the module to the db and returns it.
     *
     * @param module the module to be added
     * @return the added module
     * @throws DuplicateModuleException if the id generation fails and a duplicate id is created
     */
    public Module create(final Module module) throws DuplicateModuleException {
        final LocalDateTime time = java.time.LocalDateTime.now();
        if (module.getId() == null) {
            module.setId(UUID.randomUUID());
            module.setDateCreated(time);
        }
        if (findByID(module.getId()) == null) {
            module.setDateUpdated(time);
            em.persist(module);
        } else {
            throw new DuplicateModuleException();
        }
        return module;
    }

    /**
     * Finds and returns the module with the id specified in id.
     *
     * @param id id of the module to be found
     * @return the found module or null if no such module is found
     */
    public Module findByID(final UUID id) {
        final Module mol = em.find(Module.class, id);
        return mol;
    }

    /**
     * Finds and returns all modules.
     *
     * @return a list of all modules
     */
    public List<Module> get() {
        return em.createQuery("Select n from Module n").getResultList();
    }

    /**
     * Deletes and returns the module with the id specified in id.
     *
     * @param id id of the module to be deleted
     * @return the deleted module
     */
    public Module delete(final UUID id) {
        final Module mol = findByID(id);
        synchronized (Module.class) {
            mol.getOwner().getMyModules().remove(mol);
        }
        return mol;
    }

    /**
     * Updates the module with the id specified in id with the data of module and returns the updated module.
     *
     * @param id     id of the module to be updated
     * @param module module object that contains new data
     * @return the updated module
     */
    public Module update(final UUID id, final Module module) {
        final Module moduleToUpdate = findByID(id);
        if (moduleToUpdate != null) {
            try {
                module.setId(moduleToUpdate.getId());
                module.setDateCreated(moduleToUpdate.getDateCreated());
                em.remove(moduleToUpdate);
                create(module);
                return module;
            } catch (final DuplicateModuleException e) {
                LOGGER.error("could not update module {}", module.getName(), e);
            }
        }
        return null;
    }
}
