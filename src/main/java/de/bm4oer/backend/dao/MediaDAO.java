package de.bm4oer.backend.dao;

import de.bm4oer.backend.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

/**
 * DAO for objects of media class and media subclasses
 *
 * @author hama@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class MediaDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(MediaDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Gets all medias in the database, if the searchId is not null the function applySearch is called. As many
     * results as results specifies are returned, default is 10.
     *
     * @param cursor   id of the last object of the last page
     * @param results  defines how many objects are returned
     * @param searchId id of the search object that specifies search term, filters and sorting
     * @return media objects
     */
    public List<Media> findAll(final UUID cursor, final int results, final UUID searchId) {
        String hql = "Select n from Media n ";

        if (searchId != null) {
            final String search = applySearch(searchId);
            if (search == null) {
                return null;
            } else {
                hql = search;
            }
        } else {
            hql = hql + " ORDER BY title asc";
        }
        final Query query = em.createQuery(hql);
        final List<Media> medias = query.getResultList();
        if (cursor != null) {
            for (final Media media : medias) {
                if (media.getId().equals(cursor)) {
                    if (medias.size() >= medias.indexOf(media) + results) {
                        return medias.subList(medias.indexOf(media) + 1, medias.indexOf(media) + results + 1);
                    } else {
                        return medias.subList(medias.indexOf(media) + 1, medias.size());
                    }
                }
            }
        }
        if (medias.size() >= results) {
            return medias.subList(0, results);
        } else {
            return medias.subList(0, medias.size());
        }
    }

    /**
     * Finds and returns the media with the id specified in id.
     *
     * @param id id of the media to be found
     * @return media with the id or null
     */
    public Media find(final UUID id) {
        final Media media = em.find(Media.class, id);
        return media;
    }

    /**
     * Deletes and returns the media with the id specified in id.
     *
     * @param id id of the media to be deleted
     * @return the media that has been deleted
     */
    public Media delete(final UUID id) {
        final Media media = em.find(Media.class, id);
        if (media != null) {
            synchronized (Media.class) {
                removeFavorites(media);
                media.getOwner().getMyMedia().remove(media);
            }
            return media;
        }
        return null;
    }

    /**
     * Creates and returns a query string that applies the filters, the search term and the sorting of the search
     * object with the id specified in searchId.
     *
     * @param searchId id of the search object that contains search term, filters and sorting
     * @return the query string
     */
    private String applySearch(final UUID searchId) {
        final Search search = em.find(Search.class, searchId);
        String hql = "From Media m ";
        if (search == null) {
            return null;
        }
        if (search.getSearchTerm() != null) {
            hql = hql + "WHERE ";
            final String[] searchTerms = getSearchTerms(search);
            String terms = "(";
            String tags = " OR ";
            for (final String searchTerm : searchTerms) {
                if (searchTerms[searchTerms.length - 1] == searchTerm) {
                    terms = terms + "m.title like '%" + searchTerm + "%'";
                    tags = tags + "'" + searchTerm + "' in elements(m.tags))";
                    hql = hql + terms + tags;
                }
                terms = terms + "m.title = '%" + searchTerm + "%' OR ";
                tags = tags + "'" + searchTerm + "' in elements(m.tags) OR ";
            }
        }
        if (search.getFilters().size() != 0) {
            String filters = "";
            if (search.getSearchTerm() != null) {
                filters = " AND ";
            } else {
                hql = hql + "WHERE ";
            }
            for (final Filter f : search.getFilters()) {
                switch (f.getClass().getSimpleName()) {
                    case "ClassFilter": {
                        final ClassFilter filter = (ClassFilter) f;
                        for (int i = filter.getFromClass(); i <= filter.getToClass(); i++) {
                            if (i == filter.getFromClass()) {
                                if (i == filter.getToClass()) {
                                    filters = filters + "'" + i + "' in elements(m.gradeLevels)";
                                } else {
                                    filters = filters + "('" + i + "' in elements(m.gradeLevels) OR";
                                }
                            } else {
                                if (i == filter.getToClass()) {
                                    filters = filters + " '" + i + "' in elements(m.gradeLevels))";
                                } else {
                                    filters = filters + " '" + i + "' in elements(m.gradeLevels) OR";
                                }
                            }
                        }
                        break;
                    }
                    case "DateFilter": {
                        final DateFilter filter = (DateFilter) f;
                        filters = filters +
                                  "('" +
                                  filter.getFromDate() +
                                  "' <= m.dateUpdated AND '" +
                                  filter.getToDate() +
                                  "' >= m.dateUpdated)";
                        break;
                    }
                    case "FormatFilter": {
                        final FormatFilter filter = (FormatFilter) f;
                        for (final Format format : filter.getFileFormat()) {
                            if (filter.getFileFormat().indexOf(format) == 0) {
                                if (filter.getFileFormat().indexOf(format) ==
                                    filter.getFileFormat().size() - 1) {
                                    filters = filters + "m.format = '" + format + "'";
                                } else {
                                    filters = filters + "( m.format = '" + format + "' OR ";
                                }
                            } else {
                                if (filter.getFileFormat().indexOf(format) ==
                                    filter.getFileFormat().size() - 1) {
                                    filters = filters + "m.format = '" + format + "')";
                                } else {
                                    filters = filters + "m.format = '" + format + "' OR ";
                                }
                            }
                        }
                        break;
                    }
                    case "MaterialTypeFilter": {
                        final MaterialTypeFilter filter = (MaterialTypeFilter) f;
                        for (final MaterialType materialType : filter.getMaterialType()) {
                            if (filter.getMaterialType().indexOf(materialType) == 0) {
                                if (filter.getMaterialType().indexOf(materialType) ==
                                    filter.getMaterialType().size() - 1) {
                                    filters = filters + "m.materialType = '" + materialType + "'";
                                } else {
                                    filters = filters + "( m.materialType = '" + materialType + "' OR ";
                                }
                            } else {
                                if (filter.getMaterialType().indexOf(materialType) ==
                                    filter.getMaterialType().size() - 1) {
                                    filters = filters + "m.materialType = '" + materialType + "')";
                                } else {
                                    filters = filters + "m.materialType = '" + materialType + "' OR ";
                                }
                            }
                        }
                        break;
                    }
                    case "SubjectFilter": {
                        final SubjectFilter filter = (SubjectFilter) f;
                        filters = filters + "'" + filter.getSubjects() + "' in elements(m.subjects)";
                        break;
                    }
                    case "IdFilter": {
                        final IdFilter filter = (IdFilter) f;
                        filters = filters + "m.id = '" + filter.getSearchId() + "'";
                        break;
                    }
                }
                if (search.getFilters().indexOf(f) != search.getFilters().size() - 1) {
                    filters = filters + " AND ";
                }
                if (search.getFilters().indexOf(f) == search.getFilters().size() - 1) {
                    hql = hql + filters;
                }
            }
        }
        if (search.getSort() != null) {
            String sortHQL = " ORDER BY ";
            switch (search.getSort()) {
                case DATE_ASC: {
                    sortHQL = sortHQL + "dateCreated asc";
                    break;
                }
                case DATE_DESC: {
                    sortHQL = sortHQL + "dateCreated desc";
                    break;
                }
                case RATING_ASC: {
                    sortHQL = sortHQL + "rating asc";
                    break;
                }
                case RATING_DESC: {
                    sortHQL = sortHQL + "rating desc";
                    break;
                }
                case DOWNLOADS_ASC: {
                    sortHQL = sortHQL + "downloads asc";
                    break;
                }
                case DOWNLOADS_DESC: {
                    sortHQL = sortHQL + "downloads desc";
                    break;
                }
                case NAME_ASC: {
                    sortHQL = sortHQL + "title asc";
                    break;
                }
                case NAME_DESC: {
                    sortHQL = sortHQL + "title desc";
                    break;
                }
            }
            hql = hql + sortHQL;
        } else {
            hql = hql + " ORDER BY title asc";
        }
        return hql;
    }

    /**
     * Splits the search term of the search object at every whitespace and returns the parts as an array.
     *
     * @param s the search object
     * @return array of search terms
     */
    private String[] getSearchTerms(final Search s) {
        return s.getSearchTerm().split(" ");
    }

    /**
     * Increases the download counter of the media with the id by one.
     *
     * @param id id of the media that has been downloaded
     */
    public void incrementDownloads(final UUID id) {
        final Media m = find(id);
        final int incrementedDownloads = m.getDownloads() + 1;
        m.setDownloads(incrementedDownloads);
        em.persist(m);
    }

    /**
     * Removes the media from the favorite lists of all users.
     *
     * @param media the media that is removed
     */
    private void removeFavorites(final Media media) {
        final List<User> list = em.createQuery("Select u from User u").getResultList();
        for (final User u : list) {
            u.getFavourites().remove(media);
        }
    }
}
