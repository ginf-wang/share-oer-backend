package de.bm4oer.backend.dao;

import de.bm4oer.backend.model.Info;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

/**
 * DAO for objects of info class.
 *
 * @author jlk@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class InfoDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * Returns the last rebuild time.
     *
     * @return the last rebuild time
     */
    public LocalDateTime getRebuildTime() {
        return em.find(Info.class, 1).getRebuildTime();
    }

    /**
     * Persists a info object with id 1.
     *
     * @param rebuildTime the last rebuild time
     */
    public void setRebuildTime(final LocalDateTime rebuildTime) {
        final Info infos = em.find(Info.class, 1);
        if (infos != null) {
            em.remove(infos);
        }
        em.persist(new Info(1, rebuildTime));
    }
}
