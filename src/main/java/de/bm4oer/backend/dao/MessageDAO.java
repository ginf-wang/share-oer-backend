package de.bm4oer.backend.dao;

import de.bm4oer.backend.model.Message;
import de.bm4oer.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.annotation.RequestScope;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * DAO for objects of message class.
 *
 * @author leowallb@uni-bremen.de
 * @author jlk@uni-bremen.de
 */
@Transactional
@Named
@RequestScope
public class MessageDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the message to the db and returns it.
     *
     * @param message the message to be added
     * @return the added message
     */
    public Message create(final Message message) {
        final LocalDateTime time = java.time.LocalDateTime.now();
        if (message.getId() == null) {
            message.setId(UUID.randomUUID());
            message.setDateSent(time);
        }
        if (findByID(message.getId()) == null) {
            message.setDateSent(time);
            em.persist(message);
        }
        return message;
    }

    /**
     * Finds and returns the message with the id specified in id.
     *
     * @param id id of the message to be found
     * @return the found message or null if no such message is found
     */
    public Message findByID(final UUID id) {
        final Message message = em.find(Message.class, id);
        return message;
    }

    /**
     * Finds and returns all messages as an list of lists of messages aggregated by the users.
     *
     * @return a list of lists of messages aggregated by the users.
     */
    public List<List<Message>> get() {
        final List<List<Message>> result = new ArrayList<>();
        final List<Message> messages = em.createQuery("Select m From Message m").getResultList();
        final List<User> users =
                em.createQuery("Select u From User u Where u.username != 'Mervi'").getResultList();
        for (final User u : users) {
            result.add(messages.stream()
                               .filter(message -> message.getReceiver() == u || message.getSender() == u)
                               .collect(Collectors.toList()));
        }
        return result.stream().distinct().collect(Collectors.toList());
    }

    /**
     * Deletes and returns the message with the id specified in id.
     *
     * @param id id of the message to be deleted
     * @return the deleted message
     */
    public Message delete(final UUID id) {
        final Message message = findByID(id);
        em.remove(message);
        return message;
    }

    /**
     * Updates the message with the id specified in id with the data of message and returns the updated message.
     *
     * @param id      id of the message to be updated
     * @param message object that contains new data
     * @return the updated message
     */
    public Message update(final UUID id, final Message message) {
        final Message messageToUpdate = findByID(id);
        if (messageToUpdate != null) {
            message.setId(messageToUpdate.getId());
            message.setDateSent(messageToUpdate.getDateSent());
            em.remove(messageToUpdate);
            create(message);
        }
        return message;
    }
}
