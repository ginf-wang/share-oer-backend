package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateSearchException;
import de.bm4oer.backend.model.Search;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * DAO for objects of search class.
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class SearchDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the search to the db and returns it.
     *
     * @param search the search to be added
     * @return the added search
     * @throws DuplicateSearchException if the id generation fails and a duplicate id is created
     */
    public Search create(final Search search) throws DuplicateSearchException {
        if (search.getId() == null) {
            search.setId(UUID.randomUUID());
        }
        if (search != null) {
            synchronized (Search.class) {
                if (find(search.getId()) == null) {
                    em.persist(search);
                    return search;
                } else {
                    final DuplicateSearchException duplicateSearchException = new DuplicateSearchException();
                    throw duplicateSearchException;
                }
            }
        }
        return null;
    }

    /**
     * Finds and returns all searches.
     *
     * @return a list of all searches
     */
    public List<Search> getAllSearches() {
        final Query query = em.createQuery("SELECT n from Search n");
        final List<Search> searches = query.getResultList();
        return searches;
    }

    /**
     * Finds and returns the search with the id specified in id.
     *
     * @param id id of the search to be found
     * @return the found search or null if no such search is found
     */
    public Search find(final UUID id) {
        final Search search = em.find(Search.class, id);
        return search;
    }

    /**
     * Deletes and returns the search with the id specified in id.
     *
     * @param id id of the search to be deleted
     * @return the deleted search
     */
    public Search delete(final UUID id) {
        final Search search = find(id);
        if (Objects.nonNull(search)) {
            synchronized (Search.class) {
                em.remove(search);
                return search;
            }
        }
        return null;
    }
}
