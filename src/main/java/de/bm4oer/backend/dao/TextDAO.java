package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateTextException;
import de.bm4oer.backend.model.Text;
import de.bm4oer.backend.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * DAO for objects of text class.
 *
 * @author decker1@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class TextDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the text to the db and returns it.
     *
     * @param text the text to be added
     * @return the added text
     * @throws DuplicateTextException if the id generation fails and a duplicate id is created
     */
    public Text create(final Text text) throws DuplicateTextException {
        final LocalDateTime time = java.time.LocalDateTime.now();
        if (text.getId() == null) {
            text.setId(UUID.randomUUID());
            text.setDateCreated(time);
        }
        if (find(text.getId()) == null) {
            text.setDateUpdated(time);
            em.persist(text);
        } else {
            throw new DuplicateTextException();
        }
        return text;
    }

    /**
     * Finds and returns the text with the id specified in id.
     *
     * @param id id of the text to be found
     * @return the found text or null if no such text is found
     */
    public Text find(final UUID id) {
        final Text text = em.find(Text.class, id);
        return text;
    }

    /**
     * Updates the text with the id specified in id with the data of text and returns the updated text.
     *
     * @param id   id of the text to be updated
     * @param text text object that contains new data
     * @return the updated text
     */
    public Text update(final UUID id, final Text text) {
        final Text oldtext = find(id);
        if (text != null) {
            synchronized (Text.class) {
                text.setId(oldtext.getId());
                text.setDateCreated(oldtext.getDateCreated());
                text.setDateUpdated(java.time.LocalDateTime.now());
                em.merge(text);
                return text;
            }
        }
        return null;
    }

    /**
     * Deletes and returns the text with the id specified in id.
     *
     * @param id id of the text to be deleted
     * @return the deleted text
     */
    public Text delete(final UUID id) {
        final Text text = em.find(Text.class, id);
        if (text != null) {
            synchronized (Text.class) {
                removeFavorites(text);
                text.getOwner().getMyMedia().remove(text);
            }
            return text;
        }
        return null;
    }

    /**
     * Finds and returns all texts.
     *
     * @return the found texts.
     */
    public List<Text> getAll() {
        return em.createQuery("Select n from Text n").getResultList();
    }

    /**
     * Increases the download counter of the text with the id by one.
     *
     * @param id id of the text that has been downloaded
     */
    public void incrementDownloads(final UUID id) {
        final Text t = find(id);
        final int incrementedDownloads = t.getDownloads() + 1;
        t.setDownloads(incrementedDownloads);
        em.persist(t);
    }

    /**
     * Removes the text from the favorite lists of all users.
     *
     * @param text the text that is removed
     */
    private void removeFavorites(final Text text) {
        final List<User> list = em.createQuery("Select u from User u").getResultList();
        for (final User u : list) {
            u.getFavourites().remove(text);
        }
    }
}
