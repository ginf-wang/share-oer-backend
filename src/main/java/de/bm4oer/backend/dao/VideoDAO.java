package de.bm4oer.backend.dao;

import de.bm4oer.backend.exception.DuplicateVideoException;
import de.bm4oer.backend.model.User;
import de.bm4oer.backend.model.Video;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * DAO for objects of video class.
 *
 * @author decker1@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class VideoDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(VideoDAO.class);

    @PersistenceContext
    private EntityManager em;

    /**
     * Adds the video to the db and returns it.
     *
     * @param video the video to be added
     * @return the added video
     * @throws DuplicateVideoException if the id generation fails and a duplicate id is created
     */
    public Video create(final Video video) throws DuplicateVideoException {
        final LocalDateTime time = java.time.LocalDateTime.now();
        if (video.getId() == null) {
            video.setId(UUID.randomUUID());
            video.setDateCreated(time);
        }
        if (find(video.getId()) == null) {
            video.setDateUpdated(time);
            em.persist(video);
        } else {
            throw new DuplicateVideoException();
        }
        return video;
    }

    /**
     * Finds and returns the video with the id specified in id.
     *
     * @param id id of the video to be found
     * @return the found video or null if no such video is found
     */
    public Video find(final UUID id) {
        final Video video = em.find(Video.class, id);
        return video;
    }

    /**
     * Deletes and returns the video with the id specified in id.
     *
     * @param id id of the video to be deleted
     * @return the deleted video
     */
    public Video delete(final UUID id) {
        final Video video = em.find(Video.class, id);
        if (video != null) {
            synchronized (Video.class) {
                removeFavorites(video);
                video.getOwner().getMyMedia().remove(video);
                return video;
            }
        }
        return null;
    }

    /**
     * Updates the video with the id specified in id with the data of video and returns the updated video.
     *
     * @param id    id of the video to be updated
     * @param video video object that contains new data
     * @return the updated video
     */
    public Video update(final UUID id, final Video video) {
        final Video oldvideo = find(id);
        if (video != null) {
            synchronized (Video.class) {
                video.setId(oldvideo.getId());
                video.setDateCreated(oldvideo.getDateCreated());
                video.setDateUpdated(java.time.LocalDateTime.now());
                em.merge(video);
                return video;
            }
        }
        return null;
    }

    /**
     * Finds and returns all videos.
     *
     * @return the found videos.
     */
    public List<Video> getAll() {
        return em.createQuery("Select n from Video n").getResultList();
    }

    /**
     * Increases the download counter of the video with the id by one.
     *
     * @param id id of the video that has been downloaded
     */
    public void incrementDownloads(final UUID id) {
        final Video v = find(id);
        final int incrementedDownloads = v.getDownloads() + 1;
        v.setDownloads(incrementedDownloads);
        em.persist(v);
    }

    /**
     * Removes the video from the favorite lists of all users.
     *
     * @param video the video that is removed
     */
    private void removeFavorites(final Video video) {
        final List<User> list = em.createQuery("Select u from User u").getResultList();
        for (final User u : list) {
            u.getFavourites().remove(video);
        }
    }
}

