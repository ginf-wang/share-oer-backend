package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Abstract filter which specifies its subtypes
 *
 * @author decker1@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@Entity
@Table
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
                      @JsonSubTypes.Type(value = ClassFilter.class, name = "class"),
                      @JsonSubTypes.Type(value = DateFilter.class, name = "date"),
                      @JsonSubTypes.Type(value = FormatFilter.class, name = "format"),
                      @JsonSubTypes.Type(value = MaterialTypeFilter.class, name = "materialtype"),
                      @JsonSubTypes.Type(value = SubjectFilter.class, name = "subject"),
                      @JsonSubTypes.Type(value = IdFilter.class, name = "id")
              })
public abstract class Filter {

    @Id
    @Type(type = "uuid-char")
    private UUID id;

    public Filter() {
    }

    public Filter(final UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }
}
