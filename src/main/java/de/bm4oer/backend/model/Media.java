package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * data representation of media objects
 *
 * @author hama@uni-bremen.de
 * @author iapachi@uni-bremen.de
 * @author jlk@uni-bremen.de
 */
@Entity
@Table
public abstract class Media implements Serializable {
    @Id
    @Type(type = "uuid-char")
    @Column(updatable = false)
    private UUID id;
    @NotNull
    private String title;
    private String description;
    @Column(updatable = false)
    private String fileName;
    private double rating;
    private int downloads;

    @JsonView(View.Internal.class)
    @ManyToOne
    @NotNull
    private User owner;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(updatable = false)
    private MaterialType materialType;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(updatable = false)
    private Format format;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Column(updatable = false)
    private License license;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotNull
    private List<Subject> subjects;
    @Column(updatable = false)
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotNull
    private List<String> tags;

    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    @ElementCollection(fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @NotNull
    private List<Integer> gradeLevels;

    public Media() {
    }

    Media(final UUID id,
          final String title,
          final String description,
          final String fileName,
          final double rating,
          final int downloads,
          final User owner,
          final List<Subject> subjects,
          final LocalDateTime dateCreated,
          final LocalDateTime dateUpdated,
          final List<String> tags,
          final List<Integer> gradeLevels,
          final MaterialType materialType,
          final License license,
          final Format format) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.fileName = fileName;
        this.rating = rating;
        this.downloads = downloads;
        this.owner = owner;
        this.subjects = subjects;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.tags = tags;
        this.gradeLevels = gradeLevels;
        this.materialType = materialType;
        this.license = license;
        this.format = format;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(final double rating) {
        this.rating = rating;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setDownloads(final int downloads) {
        this.downloads = downloads;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(final List<Subject> subjects) {
        this.subjects = subjects;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(final LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(final List<String> tags) {
        this.tags = tags;
    }

    public List<Integer> getGradeLevels() {
        return gradeLevels;
    }

    public void setGradeLevels(final List<Integer> gradeLevels) {
        this.gradeLevels = gradeLevels;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String filePath) {
        fileName = filePath;
    }

    public MaterialType getMaterialType() {
        return materialType;
    }

    public void setMaterialType(final MaterialType materialType) {
        this.materialType = materialType;
    }

    public License getLicense() {
        return license;
    }

    public void setLicense(final License license) {
        this.license = license;
    }

    public Format getFormat() { return format; }

    public void setFormat(final Format format) { this.format = format; }
}
