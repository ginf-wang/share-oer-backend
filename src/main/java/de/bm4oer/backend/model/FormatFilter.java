package de.bm4oer.backend.model;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.util.List;
import java.util.UUID;

/**
 * Filter entity with a list of fileformats
 *
 * @author hama@uni-bremen.de
 */
@Entity
public class FormatFilter extends Filter {

    @ElementCollection
    private List<Format> fileFormat;

    public FormatFilter() {
    }

    public FormatFilter(final List<Format> fileFormat, final UUID id) {
        super(id);
        this.fileFormat = fileFormat;
    }

    public List<Format> getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(final List<Format> fileFormat) {
        this.fileFormat = fileFormat;
    }
}
