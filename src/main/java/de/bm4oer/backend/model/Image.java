package de.bm4oer.backend.model;

import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * model representation of images as media-objects
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 */
@Entity
public class Image extends Media {

    public Image() {

    }

    public Image(final UUID id,
                 final String title,
                 final String description,
                 final String fileName,
                 final double rating,
                 final int downloads,
                 final User owner,
                 final List<Subject> subjects,
                 final LocalDateTime dateCreated,
                 final LocalDateTime dateUpdated,
                 final List<String> tags,
                 final List<Integer> gradeLevels,
                 final MaterialType materialType,
                 final License license,
                 final Format format) {
        super(id,
              title,
              description,
              fileName,
              rating,
              downloads,
              owner,
              subjects,
              dateCreated,
              dateUpdated,
              tags,
              gradeLevels,
              materialType,
              license,
              format);
    }
}
