package de.bm4oer.backend.model;

/**
 * model representation of attribute exposure
 *
 * @author hama@uni-bremen.de
 */
public class View {

    public static class Public {
    }

    public static class InternalUser extends Public {
    }

    public static class Internal extends Public {
    }
}

