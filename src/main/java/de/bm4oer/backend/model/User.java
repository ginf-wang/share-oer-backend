package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * model representation of users with profile data, usage history and media data
 *
 * @author hama@uni-bremen.de
 * @author leowallb@uni-bremen.de
 * @author jlk@uni-bremen.de
 */
@Entity
@Table
public class User {
    @Id
    @NotNull
    @Column(updatable = false)
    @JsonView(View.InternalUser.class)
    private String email;
    @NotNull
    private String username;
    private String picture;
    @Column(updatable = false)
    @JsonView(View.InternalUser.class)
    private LocalDateTime dateCreated;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @JsonView(View.InternalUser.class)
    private List<Module> myModules;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @JsonView(View.InternalUser.class)
    private List<Media> myMedia;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JsonView(View.InternalUser.class)
    private List<Media> favourites;

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn
    @JsonView(View.InternalUser.class)
    private List<User> contacts;

    @JsonView(View.InternalUser.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "receiver", orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    private List<Message> messagesReceiver;

    @JsonView(View.InternalUser.class)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sender", orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    private List<Message> messagesSender;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn
    @JsonView(View.InternalUser.class)
    private List<Search> searchHistory;

    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn
    @JsonView(View.InternalUser.class)
    private List<Search> savedSearches;

    public User() {
    }

    public User(final String email,
                final String username,
                final String picture,
                final LocalDateTime dateCreated,
                final List<Module> myModules,
                final List<Media> myMedia,
                final List<Media> favourites,
                final List<User> contacts,
                final List<Message> messagesReceiver,
                final List<Message> messagesSender,
                final List<Search> searchHistory,
                final List<Search> savedSearches) {
        this.email = email;
        this.username = username;
        this.picture = picture;
        this.dateCreated = dateCreated;
        this.myModules = myModules;
        this.myMedia = myMedia;
        this.favourites = favourites;
        this.contacts = contacts;
        this.messagesReceiver = messagesReceiver;
        this.messagesSender = messagesSender;
        this.searchHistory = searchHistory;
        this.savedSearches = savedSearches;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<Module> getMyModules() { return myModules; }

    public void setMyModules(final List<Module> myModules) { this.myModules = myModules; }

    public List<Media> getMyMedia() { return myMedia; }

    public void setMyMedia(final List<Media> myMedia) { this.myMedia = myMedia; }

    public List<Media> getFavourites() { return favourites; }

    public void setFavourites(final List<Media> favourites) { this.favourites = favourites; }

    public List<User> getContacts() { return contacts; }

    public void setContacts(final List<User> contacts) { this.contacts = contacts; }

    public String getPicture() { return picture; }

    public void setPicture(final String picture) { this.picture = picture; }

    public List<Search> getSearchHistory() { return searchHistory; }

    public void setSearchHistory(final List<Search> searchHistory) { this.searchHistory = searchHistory; }

    public List<Search> getSavedSearches() { return savedSearches; }

    public void setSavedSearches(final List<Search> savedSearches) { this.savedSearches = savedSearches; }

    public List<Message> getMessagesReceiver() { return messagesReceiver; }

    public void setMessagesReceiver(final List<Message> messagesReceiver) { this.messagesReceiver = messagesReceiver; }

    public List<Message> getMessagesSender() { return messagesSender; }

    public void setMessagesSender(final List<Message> messagesSender) { this.messagesSender = messagesSender; }
}
