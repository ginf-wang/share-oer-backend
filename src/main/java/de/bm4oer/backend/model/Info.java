package de.bm4oer.backend.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * info bean with modifiable rebuildTime
 *
 * @author jlk@uni-bremen.de
 */
@Entity
@Table
public class Info implements Serializable {

    @Id
    @NotNull
    private int id;

    @NotNull
    private LocalDateTime rebuildTime;

    public Info() {}

    public Info(final int id, final LocalDateTime rebuildTime) {
        this.id = id;
        this.rebuildTime = rebuildTime;
    }

    public LocalDateTime getRebuildTime() {
        return rebuildTime;
    }
}
