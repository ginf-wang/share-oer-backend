package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * model representation of messages
 *
 * @author leowallb@uni-bremen.de
 * @author jlk@uni-bremen.de
 * @author hama@uni-bremen.de
 */
@Entity
@Table
public class Message {
    @Id
    @Column(updatable = false)
    private UUID uuid;

    @JsonView(View.Internal.class)
    @ManyToOne
    @NotNull
    private User receiver;

    @JsonView(View.Internal.class)
    @ManyToOne
    @NotNull
    private User sender;

    @NotNull
    @JsonView(View.Internal.class)
    @Column(length = 2000)
    private String message;

    @Column(updatable = false)
    private LocalDateTime dateSent;

    public Message() {
    }

    public Message(final UUID uuid,
                   final User receiver,
                   final User sender,
                   final String message,
                   final LocalDateTime dateSent) {
        this.uuid = uuid;
        this.receiver = receiver;
        this.sender = sender;
        this.message = message;
        this.dateSent = dateSent;
    }

    public UUID getId() {
        return uuid;
    }

    public void setId(final UUID uuid) {
        this.uuid = uuid;
    }

    public LocalDateTime getDateSent() {
        return dateSent;
    }

    public void setDateSent(final LocalDateTime dateSent) {
        this.dateSent = dateSent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(final User receiver) {
        this.receiver = receiver;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(final User sender) {
        this.sender = sender;
    }
}
