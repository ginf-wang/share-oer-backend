package de.bm4oer.backend.model;

/**
 * Accepted subjects
 *
 * @author iapachi@uni-bremen.de
 */
public enum Subject {
    BIOLOGIE,
    CHEMIE,
    DEUTSCH,
    ENGLISCH,
    MATHE,
    MUSIK,
    FRANZOESISCH,
    SPANISCH,
    INFORMATIK,
    BWL,
    KUNST,
    GESCHICHTE,
    RELIGION,
    ETHIK,
    SPORT,
    PHYSIK,
    ERDKUNDE,
    SONSTIGES
}
