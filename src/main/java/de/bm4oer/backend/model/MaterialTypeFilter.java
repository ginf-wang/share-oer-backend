package de.bm4oer.backend.model;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;
import java.util.UUID;

/**
 * Filter entity with a list of materialtypes
 *
 * @author hama@uni-bremen.de
 */
@Entity
public class MaterialTypeFilter extends Filter {

    @Enumerated(EnumType.STRING)
    @ElementCollection
    private List<MaterialType> materialType;

    public MaterialTypeFilter() {
    }

    public MaterialTypeFilter(final List<MaterialType> materialType, final UUID id) {
        super(id);
        this.materialType = materialType;
    }

    public List<MaterialType> getMaterialType() {
        return materialType;
    }

    public void setMaterialType(final List<MaterialType> materialType) {
        this.materialType = materialType;
    }
}
