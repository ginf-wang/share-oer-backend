package de.bm4oer.backend.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import java.util.UUID;

/**
 * Filter entity with an UUID
 *
 * @author decker1@uni-bremen.de
 */
@Entity
public class IdFilter extends Filter {

    @Type(type = "uuid-char")
    private UUID searchId;

    public IdFilter() {
    }

    public IdFilter(final UUID id, final UUID searchId) {
        super(id);
        this.searchId = searchId;
    }

    public UUID getSearchId() {
        return searchId;
    }

    public void setSearchId(final UUID searchId) {
        this.searchId = searchId;
    }
}
