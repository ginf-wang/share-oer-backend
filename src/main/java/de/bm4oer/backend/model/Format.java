package de.bm4oer.backend.model;

/**
 * accepted file formats
 *
 * @author decker1@uni-bremen.de
 */
public enum Format {
    pdf,
    mp4,
    png,
    odt,
    txt,
    jpg
}
