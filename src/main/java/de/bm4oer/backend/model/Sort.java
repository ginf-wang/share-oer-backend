package de.bm4oer.backend.model;

/**
 * Available sortings
 *
 * @author hama@uni-bremen.de
 */
public enum Sort {
    RATING_ASC,
    RATING_DESC,
    DOWNLOADS_ASC,
    DOWNLOADS_DESC,
    DATE_ASC,
    DATE_DESC,
    NAME_ASC,
    NAME_DESC
}
