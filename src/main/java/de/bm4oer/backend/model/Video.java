package de.bm4oer.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * model representation of videos as media-objects
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 */
@Entity
public class Video extends Media {
    @NotNull
    @Column(updatable = false)
    private double length;

    public Video() {
    }

    public Video(final UUID id,
                 final String title,
                 final String description,
                 final String fileName,
                 final double rating,
                 final int downloads,
                 final User owner,
                 final List<Subject> subjects,
                 final LocalDateTime dateCreated,
                 final LocalDateTime dateUpdated,
                 final List<String> tags,
                 final List<Integer> gradeLevels,
                 final double length,
                 final MaterialType materialType,
                 final License license,
                 final Format format) {
        super(id,
              title,
              description,
              fileName,
              rating,
              downloads,
              owner,
              subjects,
              dateCreated,
              dateUpdated,
              tags,
              gradeLevels,
              materialType,
              license,
              format);
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(final double length) {
        this.length = length;
    }
}
