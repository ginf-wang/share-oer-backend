package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * representation of media collections named modules
 *
 * @author hama@uni-bremen.de
 */
@Table
@Entity
public class Module {
    @JsonProperty("name")
    @NotNull
    private String name;

    @Fetch(FetchMode.JOIN)
    @JsonProperty("medias")
    @JoinColumn
    @OneToMany
    @NotNull
    private List<Media> medias;

    @Id
    @Column(updatable = false)
    private UUID id;

    @Column(updatable = false)
    private LocalDateTime dateCreated;
    private LocalDateTime dateUpdated;

    @JsonView(View.Internal.class)
    @ManyToOne
    @NotNull
    private User owner;

    public Module() {
    }

    public Module(final String name,
                  final List<Media> medias,
                  final UUID id,
                  final LocalDateTime dateCreated,
                  final LocalDateTime dateUpdated,
                  final User owner) {
        this.name = name;
        this.medias = medias;
        this.id = id;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
        this.owner = owner;
    }

    public UUID getId() { return id; }

    public void setId(final UUID id) { this.id = id; }

    public List<Media> getMedias() { return medias; }

    public void setMedias(final List<Media> media) { medias = media; }

    public User getOwner() { return owner; }

    public void setOwner(final User owner) { this.owner = owner; }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public LocalDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(final LocalDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
