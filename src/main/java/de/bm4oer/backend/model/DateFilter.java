package de.bm4oer.backend.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.UUID;

/**
 * Filter entity with date span
 *
 * @author hama@uni-bremen.de
 */
@Entity
public class DateFilter extends Filter {

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate fromDate;

    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate toDate;

    public DateFilter() {
    }

    public DateFilter(final LocalDate fromDate, final LocalDate toDate, final UUID id) {
        super(id);
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(final LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(final LocalDate toDate) {
        this.toDate = toDate;
    }
}
