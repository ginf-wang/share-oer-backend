package de.bm4oer.backend.model;

/**
 * accepted Open Content licenses
 *
 * @author hama@uni-bremen.de
 */
public enum License {
    CC_BY,
    CC_BY_SA,
    CC_BY_ND,
    CC_BY_NC,
    CC_BY_NC_SA,
    CC_BY_NC_ND
}
