package de.bm4oer.backend.model;

import javax.persistence.Entity;
import java.util.UUID;

/**
 * Filter entity with class grade span
 *
 * @author hama@uni-bremen.de
 */
@Entity
public class ClassFilter extends Filter {
    private int fromClass;
    private int toClass;

    public ClassFilter() {
    }

    public ClassFilter(final int fromClass, final int toClass, final UUID id) {
        super(id);
        this.fromClass = fromClass;
        this.toClass = toClass;
    }

    public int getFromClass() {
        return fromClass;
    }

    public void setFromClass(final int fromClass) {
        this.fromClass = fromClass;
    }

    public int getToClass() {
        return toClass;
    }

    public void setToClass(final int toClass) {
        this.toClass = toClass;
    }
}
