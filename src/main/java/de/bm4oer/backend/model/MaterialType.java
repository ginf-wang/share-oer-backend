package de.bm4oer.backend.model;

/**
 * accepted types of educational resources
 *
 * @author hama@uni-bremen.de
 */
public enum MaterialType {
    ZUSAMMENFASSUNG,
    ARBEITSBLATT,
    VORLESUNG,
    VIDEO,
    KLAUSUR,
    SPRACHAUFZEICHNUNG,
    BILD,
    SONSTIGES
}
