package de.bm4oer.backend.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

/**
 * Filter entity with a list of subjects
 *
 * @author hama@uni-bremen.de
 */
@Entity
public class SubjectFilter extends Filter {

    @Enumerated(EnumType.STRING)
    private Subject subjects;

    public SubjectFilter() {
    }

    public SubjectFilter(final UUID id, final Subject subjects) {
        super(id);
        this.subjects = subjects;
    }

    public Subject getSubjects() {
        return subjects;
    }

    public void setSubjects(final Subject subjects) {
        this.subjects = subjects;
    }
}
