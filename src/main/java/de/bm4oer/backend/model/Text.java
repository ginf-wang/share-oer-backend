package de.bm4oer.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * model representation of texts as media-objects
 *
 * @author decker1@uni-bremen.de
 * @author iapachi@uni-bremen.de
 */
@Entity
public class Text extends Media {
    @NotNull
    @Column(updatable = false)
    private int pages;

    public Text() {
    }

    public Text(final UUID id,
                final String title,
                final String description,
                final String fileName,
                final double rating,
                final int downloads,
                final User owner,
                final List<Subject> subjects,
                final LocalDateTime dateCreated,
                final LocalDateTime dateUpdated,
                final List<String> tags,
                final List<Integer> gradeLevels,
                final int pages,
                final MaterialType materialType,
                final License license,
                final Format format) {
        super(id,
              title,
              description,
              fileName,
              rating,
              downloads,
              owner,
              subjects,
              dateCreated,
              dateUpdated,
              tags,
              gradeLevels,
              materialType,
              license,
              format);
        this.pages = pages;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(final int pages) {
        this.pages = pages;
    }
}
