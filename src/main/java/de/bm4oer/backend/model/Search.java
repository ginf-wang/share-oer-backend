package de.bm4oer.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * model representation of searches as collection of filters, a searchterm and optional a sorting
 *
 * @author hama@uni-bremen.de
 */
@Entity
@Table
public class Search {
    @Id
    @Type(type = "uuid-char")
    @Column(updatable = false)
    private UUID id;

    private String searchTerm;

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Filter> filters;

    private Sort sort;

    public Search() {
    }

    public Search(final UUID id, final String searchTerm, final List<Filter> filters, final Sort sort) {
        this.id = id;
        this.searchTerm = searchTerm;
        this.filters = filters;
        this.sort = sort;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(final List<Filter> filters) {
        this.filters = filters;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(final String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public Sort getSort() { return sort; }

    public void setSort(final Sort sort) { this.sort = sort; }
}
