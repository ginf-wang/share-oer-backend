package de.bm4oer.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.Collections;

/**
 * Base class to start this server
 *
 * @author jlk@uni-bremen.de
 */
@SpringBootApplication
@EnableScheduling
public class BackendApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackendApplication.class);

    /**
     * Runs this application als SpringApplication
     *
     * @param args Given runtime arguments
     */
    public static void main(final String[] args) {
        LOGGER.info("Starting the application");
        SpringApplication.run(BackendApplication.class, args);
    }

    /**
     * Configures CORS-settings to allow used methods from all origins
     *
     * @return configuration sourced on every URL
     */
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final var configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT", "PATCH"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Collections.singletonList(CorsConfiguration.ALL));

        final var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }

    /**
     * Applies CORS-configuration
     */
    @Configuration
    public class WebConfig implements WebMvcConfigurer {
        @Override
        public void addCorsMappings(final CorsRegistry registry) {
            registry.addMapping("/**");
        }
    }

    /**
     * Configures authentication
     *
     * @author iapachi@uni-bremen.de
     */
    @Configuration
    @EnableWebSecurity
    public static class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

        public static final String username = "marv@bm4oer.de";
        public static final String password = "Schwertfisch";
        private static final String userRole = "ROLE_USER";

        /**
         * Enables HTTP Basic access authentication on given HttpSecurity
         *
         * @param httpSecurity to configure
         * @throws Exception if misconfigured
         */
        @Override
        protected void configure(final HttpSecurity httpSecurity) throws Exception {
            httpSecurity.csrf().disable()
                        .authorizeRequests().anyRequest().authenticated()
                        .and().httpBasic();
        }

        /**
         * Excludes some URIs on given WebSecurity
         *
         * @param web to configure
         */
        @Override
        public void configure(final WebSecurity web) {
            web.ignoring()
               .antMatchers("/")
               .antMatchers("/info")
               .antMatchers("/error")
               .antMatchers("/search")
               .antMatchers("/javadoc/**")
               .antMatchers("/api-doc.html")
               .antMatchers(HttpMethod.GET, "/media/**")
               .antMatchers(HttpMethod.GET, "/image/**")
               .antMatchers(HttpMethod.GET, "/video/**")
               .antMatchers(HttpMethod.GET, "/text/**")
               .antMatchers(HttpMethod.OPTIONS, "**");
        }

        /**
         * Enables test credentials concering HTTP auth
         *
         * @param authentication to configure
         * @throws Exception if misconfigured
         */
        @Autowired
        public void configureGlobal(final AuthenticationManagerBuilder authentication)
                throws Exception {
            authentication.inMemoryAuthentication()
                          .withUser(username)
                          .password(passwordEncoder().encode(password))
                          .authorities(userRole);
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }
    }
}
