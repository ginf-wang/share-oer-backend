package de.bm4oer.backend.exception;

/**
 * Thrown if filter with UUID already exists
 */
public class DuplicateFilterException extends Exception {
}
