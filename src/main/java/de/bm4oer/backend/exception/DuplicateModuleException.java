package de.bm4oer.backend.exception;

/**
 * Thrown if module with UUID already exists
 */
public class DuplicateModuleException extends Exception {
}
