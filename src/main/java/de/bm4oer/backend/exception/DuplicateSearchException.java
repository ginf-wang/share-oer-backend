package de.bm4oer.backend.exception;

/**
 * Thrown if search with UUID already exists
 */
public class DuplicateSearchException extends Exception {
}
