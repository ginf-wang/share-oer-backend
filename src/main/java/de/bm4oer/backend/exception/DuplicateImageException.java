package de.bm4oer.backend.exception;

/**
 * Thrown if image with UUID already exists
 */
public class DuplicateImageException extends Exception {
}
