package de.bm4oer.backend.exception;

/**
 * Thrown if text with UUID already exists
 */
public class DuplicateTextException extends Exception {
}
