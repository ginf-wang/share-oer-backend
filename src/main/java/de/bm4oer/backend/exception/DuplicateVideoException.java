package de.bm4oer.backend.exception;

/**
 * Thrown if video with UUID already exists
 */
public class DuplicateVideoException extends Exception {
}
