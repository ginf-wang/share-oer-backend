package de.bm4oer.backend.exception;

/**
 * Thrown if user with UUID already exists
 */
public class DuplicateUserException extends Exception {
    public DuplicateUserException() {
        super("email already in use");
    }
}
