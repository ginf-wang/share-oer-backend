package de.bm4oer.backend.utils;

import de.bm4oer.backend.model.User;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Generates predefined message conversations between given users pseudorandomly
 *
 * @author jlk@uni-bremen.de
 */
class MessageFactory {

    private final User[] users;
    private final User receiver;
    private final List<String[]> conversationStrings = Arrays.asList(
            new String[]{
                    "Moin, wie gehts?",
                    "Gut und dir?",
                    "Alles gut soweit.\nBisschen stressig gerade zur Klausurenphase",
                    "Jo bei mir auch"
            },
            new String[]{
                    "Hey, cool, dass du jetzt auch hier bist!",
                    "Ja, lohnt sich :)"
            },
            new String[]{
                    "Hey, hast du dir die Lösungen zum Blatt in Mathe?",
                    "Nee, habe Ben gefragt aber noch keine Antwort",
                    "Ok, dann mache ich das auch mal"
            },
            new String[]{
                    "Hallo, ich habe eine Spende im Wert von 4.800.000,00 EURO für Sie. Ich habe die America-Lotterie" +
                    " im Wert von 40 Millionen USD gewonnen und spende einen Teil davon an eine " +
                    "Wohltätigkeitsorganisation. " +
                    "Sie können mich über (nickcristdonation0@outlook.com) kontaktieren, um weitere Informationen" +
                    " zu erhalten."
            },
            new String[]{
                    "Haben wir was auf in Bio?",
                    "Bis in zwei Wochen sollten wir doch noch diese Augenabbildung machen",
                    "achja stimmt, thx"
            },
            new String[]{
                    "Kommst du später noch Discord?",
                    "Jo hab Bock, bisschen Fortnite dies das",
                    "lel"
            },
            new String[]{
                    "Moin",
                    "Hi\nWas gibts?",
                    "Wollen wir nach Sport morgen ins Kino?",
                    "ok. Welche Film?",
                    "Weiß noch nicht, lass uns spontan entscheiden"
            },
            new String[]{
                    "Waaaaaas geeeeeeht",
                    "Alles was Beine hat",
                    "Direkt geblockt"
            },
            new String[]{
                    "Ich wollte dir nur sagen, dass ich deine neue Haarfarbe echt gut finde",
                    "Hey vielen Dank! Deine ist aber auch toll",
                    ":)))"
            },
            new String[]{
                    "Huhu (:",
                    "Hey!",
                    "Hast du Lust morgen vorbeizukommen und wir besprechen mal PoWi?",
                    "Ja können wir machen.\nWelche Uhrzeit?",
                    "Direkt nach dem Unterricht?",
                    "Ok geht klar"
            },
            new String[]{
                    "Wer war jetzt noch genau in unserer Gruppe?",
                    "Ben, Alex und Maike",
                    "Ok dann soll ich denen schreiben?",
                    "Ja mach das gern",
                    "Gut danke"
            }
    );

    MessageFactory(final User[] users, final User receiver) {
        this.users = users;
        this.receiver = receiver;
    }

    /**
     * returns conversations via MessageContainer arrays based on initialized users
     *
     * @param count of conversations to return
     * @return list of MessageContainer arrays of requested size
     */
    List<MessageContainer[]> getConversions(final int count) {
        final List<MessageContainer[]> conversations = new ArrayList<>();
        final List<Integer> usedSenders = new ArrayList<>();
        final List<Integer> usedConversations = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            final Random seed = new Random();
            int sender = seed.nextInt(users.length);
            while (usedSenders.contains(sender)) {
                sender = seed.nextInt(users.length);
            }
            int conversationNum = seed.nextInt(conversationStrings.size());
            while (usedConversations.contains(conversationNum)) {
                conversationNum = seed.nextInt(conversationStrings.size());
            }
            final String[] conversation = conversationStrings.get(conversationNum);
            final long dateStart =
                    LocalDateTime.of(2019, Month.OCTOBER, 14, 8, 27).toEpochSecond(ZoneOffset.ofHours(2));
            final long dateEnd = LocalDateTime.now().minusDays(14).toEpochSecond(ZoneOffset.ofHours(2));
            LocalDateTime conversationTime =
                    LocalDateTime.ofEpochSecond(seed.longs(1, dateStart, dateEnd).sum(), 0, ZoneOffset.ofHours(2));
            final MessageContainer[] messages = new MessageContainer[conversation.length];
            for (int j = 0; j < conversation.length; j++) {
                conversationTime = conversationTime.plusMinutes(seed.nextInt(1440));
                if (j % 2 == 0) {
                    messages[j] = new MessageContainer(conversation[j], users[sender], receiver, conversationTime);
                } else {
                    messages[j] = new MessageContainer(conversation[j], receiver, users[sender], conversationTime);
                }
            }
            conversations.add(messages);
            usedSenders.add(sender);
            usedConversations.add(conversationNum);
        }

        return conversations;
    }

    /**
     * MessageContainer without the need of an UUID
     */
    public static class MessageContainer {
        final String message;
        final User sender;
        final User receiver;
        final LocalDateTime dateSent;

        MessageContainer(final String message,
                         final User receiver,
                         final User sender,
                         final LocalDateTime dateSent) {
            this.message = message;
            this.sender = sender;
            this.receiver = receiver;
            this.dateSent = dateSent;
        }
    }
}
