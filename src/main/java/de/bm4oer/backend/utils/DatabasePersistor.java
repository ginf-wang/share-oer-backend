package de.bm4oer.backend.utils;

import de.bm4oer.backend.model.Module;
import de.bm4oer.backend.model.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This persistor is used to (re)create demodata via schedule
 *
 * @author jlk@uni-bremen.de
 */
@Transactional
@Named
@RequestScoped
public class DatabasePersistor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabasePersistor.class);
    private static final String MEDIENPORTAL_SIEMENS = "https://medienportal.siemens-stiftung.org/lib/obj_download";
    private final SessionFactory sessionFactory;
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Configures database-session settings
     */
    public DatabasePersistor() {
        final Properties sessionConfig = new Properties();
        sessionConfig.setProperty("hibernate.connection.url", "jdbc:h2:file:./bm4oer.db;AUTO_SERVER=TRUE");
        sessionConfig.setProperty("dialect", "org.hibernate.dialect.H2Dialect");
        sessionConfig.setProperty("hibernate.connection.username", "sa");
        sessionConfig.setProperty("hibernate.connection.password", "sa");
        sessionConfig.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        sessionFactory = new Configuration().addProperties(sessionConfig).buildSessionFactory();
    }

    /**
     * truncates all public tables via autodiscovery
     */
    public void recreateDB() {

        LOGGER.info("dropping old Database");

        Transaction transaction = null;

        try (final Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.flush();
            session.clear();

            session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();

            final var tables =
                    session.createSQLQuery(
                            "select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='PUBLIC'")
                           .list();
            final List<String> filteredTables =
                    (List<String>) tables.stream().filter(s -> !s.equals("INFO")).collect(Collectors.toList());
            for (final String table : filteredTables) {
                session.createSQLQuery("TRUNCATE TABLE " + table).executeUpdate();
            }
            session.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();

            transaction.commit();

            persistDB();
        } catch (final HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            LOGGER.error("Could not recreate Database", e);
        }
    }

    /**
     * persists demodata into database
     */
    private void persistDB() {

        final UUID idSearch = UUID.fromString("ae7ada97-b296-4d21-adfc-8331b45d42fb");
        final UUID idWasserModule = UUID.fromString("7dd12cb3-38cd-40ce-82f6-099a37665b22");
        final UUID idText16 = UUID.fromString("ae7ada97-b296-4d21-adfc-8331b45d42aa");
        final UUID idText17 = UUID.fromString("ae7ada97-c396-4d21-adfc-8331b45d42bb");
        final UUID idImage18 = UUID.fromString("ae7ada97-d496-4d21-adfc-8331b45d42ba");
        final UUID idVideo19 = UUID.fromString("ae7ada97-e596-4d21-adfc-8331b45d42ca");
        final UUID idText20 = UUID.fromString("ae7ada97-f696-4d21-adfc-8331b45d42cd");
        final UUID idWassermolekuel = UUID.fromString("fc938a68-d1f3-4c75-82fd-ab48080e67b1");
        final UUID idLandWasserErde = UUID.fromString("e05b1d91-d40d-47a7-a767-0378aab773d5");
        final UUID idWassermangel = UUID.fromString("7237a5b2-1162-4b5d-9c9e-2a86ecb3eb4d");
        final UUID idVerdunstungsvorgang = UUID.fromString("16a76999-9089-4469-8aca-c2d0b624f1ca");
        final UUID idVerbrennen = UUID.fromString("60db7b53-0da4-4749-9b25-9d4d4015673a");
        final UUID marvText1 = UUID.fromString("926532ac-243c-4927-b065-204f7615a0d8");
        final UUID marvText2 = UUID.fromString("800c24ae-c1d5-4e26-a63f-3aa697d849cd");
        final UUID marvText3 = UUID.fromString("958270b7-7018-4c46-a263-5897b647edfa");
        final UUID marvText4 = UUID.fromString("dcffc282-db28-49c9-b823-7456507ea4fd");
        final UUID photosynthtext = UUID.fromString("ae7ada97-b296-4d21-adfc-8332b45d42fa");
        final UUID idEyeImage = UUID.fromString("ae7ada97-b296-4d21-adfc-8332b45d42fb");
        final UUID idontknow = UUID.fromString("ae7ada97-b296-4d21-adfc-8332b45d42fc");
        final UUID marvVideo1 = UUID.fromString("61b05f64-0786-4851-9e1f-7f0b10fd5f3b");
        final UUID idVideoExpo = UUID.fromString("ae7ada97-b296-4d21-adfc-8332b45d42fd");
        final UUID idVideoJava = UUID.fromString("ae7ada97-b296-4d21-adfc-8332b45d42fe");

        final List<Subject> subjects16 = List.of(Subject.PHYSIK, Subject.ERDKUNDE);
        final List<Subject> subjects17 = List.of(Subject.INFORMATIK);
        final List<Subject> subjects18 = List.of(Subject.PHYSIK);
        final List<Subject> subjects19 = List.of(Subject.PHYSIK, Subject.ERDKUNDE);
        final List<Subject> subjects20 = List.of(Subject.BIOLOGIE, Subject.CHEMIE);
        final List<Subject> subjectMathe = List.of(Subject.MATHE);
        final List<Subject> subjectPhysik = List.of(Subject.PHYSIK);

        final List<String> tags16 = Arrays.asList("Energie", "Gruppenarbeit");
        final List<String> tags17 = Arrays.asList("BigData", "Daten");
        final List<String> tags18 = Arrays.asList("Energie", "Wind", "Wasser");
        final List<String> tags19 = Arrays.asList("Energie", "Strom", "erneuerbar");
        final List<String> tags20 = Arrays.asList("Bakterien", "Keime");
        final List<String> tagChance = Arrays.asList("Wahrscheinlichkeit", "Zahlen", "Zufall");
        final List<String> tagTrigo = Arrays.asList("Dreieck", "Trigonometrie", "rechtwinklig");
        final List<String> tagKomplex = Arrays.asList("Komplexe Zahlen", "Imaginär", "Real");
        final List<String> tagGrundMathe =
                Arrays.asList("Grundwissen Mathe", "Logik", "Mengenlehre", "Arithmetik", "Algebra");
        final List<String> tagSolarzelle = Arrays.asList("Solarzelle", "Spannung", "Arbeitsblatt");

        final List<Integer> gradeLevels16 = Collections.singletonList(10);
        final List<Integer> gradeLevels17 = Arrays.asList(7, 8, 9, 10);
        final List<Integer> gradeLevels18 = Arrays.asList(7, 8, 9, 10);
        final List<Integer> gradeLevels19 = Arrays.asList(7, 8, 9, 10);
        final List<Integer> gradeLevels20 = Arrays.asList(5, 6, 7, 8, 9, 10, 11, 12);
        final List<Integer> gradeChance = Arrays.asList(8, 9);
        final List<Integer> gradeTrigo = Arrays.asList(8, 9, 10);
        final List<Integer> gradeKomplex = Arrays.asList(7, 8, 9);
        final List<Integer> gradeGrundMathe = Arrays.asList(5, 6, 7, 8, 9, 10);
        final List<Integer> gradeSolar = Arrays.asList(5, 6, 7, 9);

        final ClassFilter classFilter = new ClassFilter(7, 9,
                                                        UUID.fromString("29fab9a6-ec93-4bc2-84af-22f044d00dad"
                                                        ));
        final DateFilter dateFilter = new DateFilter(LocalDate.of(2019, 5, 1), LocalDate.of(2020, 5, 31),
                                                     UUID.fromString("ae516f13-3569-4151-b22f-a1aa54459a13"));
        final FormatFilter formatFilter = new FormatFilter(Collections.singletonList(Format.pdf),
                                                           UUID.fromString(
                                                                   "c33109ae-f579-40ad-8e99-a8af5b040039"));
        final SubjectFilter subjectFilter =
                new SubjectFilter(UUID.fromString("499ddebc-c932-43cd-8ffb-ef06d6d906c0"),
                                  Subject.BIOLOGIE);
        final MaterialTypeFilter materialTypeFilter =
                new MaterialTypeFilter(Collections.singletonList(MaterialType.ZUSAMMENFASSUNG),
                                       UUID.fromString("7fd8a636-9046-4c0a-a67d-44765d8a12f3"));
        final IdFilter idFilter = new IdFilter(UUID.fromString("499ddebc-c932-43cd-8ffb-ef06d6d906c1"),
                                               idontknow);
        final IdFilter idFilter2 = new IdFilter(UUID.fromString("ae7ada40-b296-4a21-adfc-8331b45d4afd"),
                                                marvText3);
        final IdFilter idFilter3 = new IdFilter(UUID.fromString("ae7ada40-b296-4b21-aefc-8331b45d4bfd"),
                                                idVideo19);
        final IdFilter idFilter4 = new IdFilter(UUID.fromString("ae7ada40-b296-4c21-aefc-8331b49d4cfd"),
                                                idImage18);
        final List<Filter> filter =
                Collections.singletonList(subjectFilter);

        final Search search2 =
                new Search(UUID.fromString("401fde26-bdfa-47ce-a8da-fc5d6a20a605"),
                           "",
                           Collections.singletonList(idFilter2),
                           null);
        final Search search3 =
                new Search(UUID.fromString("00161947-900e-4292-93a8-dd2f0869c6c7"),
                           "",
                           Collections.singletonList(idFilter3),
                           null);
        final Search search4 =
                new Search(UUID.fromString("97912b97-f5aa-4753-8b0b-e490cd4230e8"),
                           "",
                           Collections.singletonList(idFilter4),
                           null);

        final Search search = new Search(idSearch, "Photosynthese", filter, Sort.RATING_DESC);

        final User bendover =
                new User("ben@example.com",
                         "Ben_Dover",
                         null,
                         LocalDateTime.of(2020, 4, 20, 6, 10),
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null);
        final User siemens =
                new User("siemens@example.com",
                         "Siemens710",
                         null,
                         LocalDateTime.of(2020, 5, 13, 16, 9),
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null);
        final User maxMustermann =
                new User("max_mustermann@example.com",
                         "max_mustermann",
                         null,
                         LocalDateTime.of(2020, 4, 20, 6, 9),
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null,
                         null);
        final User userMarvin =
                new User("marv@bm4oer.de",
                         "Mervi",
                         null,
                         LocalDateTime.of(2020, 1, 15, 6, 3),
                         null,
                         null,
                         null,
                         Arrays.asList(bendover, siemens, maxMustermann),
                         null,
                         null,
                         null,
                         null);

        final MessageFactory messageFactory = new MessageFactory(new User[]{
                bendover, maxMustermann, siemens
        }, userMarvin);
        final Text marvTextTrigonometrie =
                new Text(marvText1,
                         "Trigonometrie",
                         null,
                         "https://henriks-mathewerkstatt.de/1522.Trigonometrie.Wissen.pdf",
                         9.3,
                         6,
                         userMarvin,
                         subjectMathe,
                         LocalDateTime.of(2020, 5, 1, 2, 2),
                         LocalDateTime.of(2020, 5, 1, 2, 2),
                         tagTrigo,
                         gradeTrigo,
                         4,
                         MaterialType.ZUSAMMENFASSUNG,
                         License.CC_BY,
                         Format.pdf);
        final Text marvTextKomplexeZahlen =
                new Text(marvText2,
                         "Komplexe Zahlen",
                         null,
                         "https://upload.wikimedia.org/wikipedia/commons/a/aa/Komplexe_Zahlen.pdf",
                         8.7,
                         158,
                         userMarvin,
                         subjectMathe,
                         LocalDateTime.of(2020, 2, 4, 2, 0),
                         LocalDateTime.of(2020, 2, 4, 2, 0),
                         tagKomplex,
                         gradeKomplex,
                         104,
                         MaterialType.ZUSAMMENFASSUNG,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text marvTextGrundwissenMathematik =
                new Text(marvText3,
                         "Grundwissen Mathematik",
                         null,
                         "https://www.grund-wissen.de/mathematik/_downloads/grundwissen-mathematik.pdf",
                         9.7,
                         619,
                         userMarvin,
                         subjectMathe,
                         LocalDateTime.of(2020, 1, 27, 16, 33),
                         LocalDateTime.of(2020, 1, 27, 16, 33),
                         tagGrundMathe,
                         gradeGrundMathe,
                         334,
                         MaterialType.ZUSAMMENFASSUNG,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text marvTextSolarzelle =
                new Text(marvText4,
                         "Ist die Solarzelle eine Spannungsquelle?",
                         null,
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=108237&fileid=1",
                         3.5,
                         300,
                         userMarvin,
                         subjectPhysik,
                         LocalDateTime.of(2020, 1, 27, 16, 33),
                         LocalDateTime.of(2020, 1, 27, 16, 33),
                         tagSolarzelle,
                         gradeSolar,
                         5,
                         MaterialType.ARBEITSBLATT,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text text16 =
                new Text(idText16,
                         "Energiesparen_Gruppenarbeit_Schueleranleitung",
                         null,
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=101251&fileid=1",
                         6.7,
                         2,
                         bendover,
                         subjects16,
                         LocalDateTime.of(2019, 5, 2, 6, 16),
                         LocalDateTime.of(2019, 5, 2, 6, 16),
                         tags16,
                         gradeLevels16,
                         2,
                         MaterialType.ARBEITSBLATT,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text text17 =
                new Text(idText17,
                         "BigData-Datenspuren",
                         null,
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=111977&fileid=1",
                         8.0,
                         120,
                         bendover,
                         subjects17,
                         LocalDateTime.of(2019, 5, 2, 6, 17),
                         LocalDateTime.of(2019, 5, 2, 6, 17),
                         tags17,
                         gradeLevels17,
                         1,
                         MaterialType.ARBEITSBLATT,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text text20 =
                new Text(idText20,
                         "Keime am Koerper",
                         null,
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=109294&fileid=1",
                         8.0,
                         76,
                         bendover,
                         subjects20,
                         LocalDateTime.of(2019, 5, 2, 6, 20),
                         LocalDateTime.of(2019, 5, 2, 6, 20),
                         tags20,
                         gradeLevels20,
                         3,
                         MaterialType.ARBEITSBLATT,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text wassermangel =
                new Text(idWassermangel,
                         "Maßnahmen und Techniken gegen Wassermangel",
                         "Maßnahmen und Techniken gegen den Wassermangel werden in Form von kurzen und prägnanten " +
                         "Lösungsansätzen vorgestellt.",
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=101551&fileid=1",
                         9.4,
                         98,
                         siemens,
                         Arrays.asList(Subject.CHEMIE, Subject.PHYSIK, Subject.ETHIK),
                         LocalDateTime.of(2020, 5, 14, 20, 40),
                         LocalDateTime.of(2020, 5, 14, 20, 40),
                         Arrays.asList("Trinkwasser", "Wasseraufbereitung", "Wasserversorgung", "Zukunft"),
                         Arrays.asList(7, 8, 9),
                         8,
                         MaterialType.ZUSAMMENFASSUNG,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text verdunstungsvorgang =
                new Text(idVerdunstungsvorgang,
                         "Der Verdunstungsvorgang",
                         "Die Verdunstung bildet ein Element des Wasserkreislaufs. Die Verdunstung führt den" +
                         " Wasserdampf in höhere, kalte Luftschichten, wo er in Form kleiner Wassertröpfchen oder" +
                         " Eiskriställchen Wolken bildet, die wieder zur Erde abregnen..",
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=101356&fileid=1",
                         3.1,
                         37,
                         siemens,
                         Arrays.asList(Subject.CHEMIE, Subject.PHYSIK),
                         LocalDateTime.of(2020, 5, 14, 20, 40),
                         LocalDateTime.of(2020, 5, 14, 20, 40),
                         Arrays.asList("Aggregatzustande", "Niederschlag", "Wasser", "Wasserkreislauf"),
                         Arrays.asList(5, 6),
                         1,
                         MaterialType.ARBEITSBLATT,
                         License.CC_BY_SA,
                         Format.pdf);
        final Text photosynthesisText =
                new Text(photosynthtext,
                         "Photosynthese",
                         "Dieser Leitfaden gibt einen Überblick über den inhaltlichen und didaktischenZusammenhang " +
                         "der " +
                         "Medien des Medienpakets Photosynthese",
                         MEDIENPORTAL_SIEMENS +
                         ".php?objid=103732&fileid=1",
                         7.3,
                         49,
                         maxMustermann,
                         List.of(Subject.BIOLOGIE),
                         LocalDateTime.of(2019, 5, 13, 16, 14),
                         LocalDateTime.of(2019, 5, 13, 16, 14),
                         Arrays.asList("Photosynthese", "Sauerstoff", "Stoffwechselprozess"),
                         Arrays.asList(7),
                         3,
                         MaterialType.ZUSAMMENFASSUNG,
                         License.CC_BY_SA,
                         Format.pdf);
        final Image eyeImage =
                new Image(idEyeImage,
                          "Das Auge",
                          "Aufbau und Funktion",
                          "https://assets.serlo.org/legacy/58c1736631932_443e7756ce97ad1f3802d68da9855ed8b6a97451.png",
                          9.9,
                          420,
                          maxMustermann,
                          List.of(Subject.BIOLOGIE),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          Arrays.asList("Auge",
                                        "Lederhaut",
                                        "Aderhaut",
                                        "Hornhaut",
                                        "Pupille",
                                        "Augenkammer",
                                        "Iris",
                                        "Ziliarmuskel",
                                        "Augenlise",
                                        "Glaskörper",
                                        "Netzhaut",
                                        "Sehnerv",
                                        "Blinder Fleck",
                                        "Gelber Fleck"),
                          Arrays.asList(9),
                          MaterialType.BILD,
                          License.CC_BY_SA,
                          Format.png);
        final Image earImage =
                new Image(idontknow,
                          "Das Ohr",
                          "Das Ohr",
                          "https://assets.serlo.org/legacy/583186e57b35a_7778c5de32ebd0b087ae0ee11665a229f1760d20.png",
                          9.6,
                          329,
                          maxMustermann,
                          List.of(Subject.BIOLOGIE),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          Arrays.asList("äußerer Gehörgang",
                                        "Trommelfell",
                                        "Paukenhöhle",
                                        "rundes Fenster",
                                        "Ohrtrompete",
                                        "Hirnnerv",
                                        "Bogengang",
                                        "ovales Fenster",
                                        "Steigbügel",
                                        "Amboss",
                                        "Hammer"),
                          Arrays.asList(9),
                          MaterialType.BILD,
                          License.CC_BY_SA,
                          Format.png);
        final Image wassermolekuehl =
                new Image(idWassermolekuel,
                          "Das Wassermolekül",
                          "Grafische Darstellung eines Wasserstoffmoleküls",
                          MEDIENPORTAL_SIEMENS +
                          ".php?objid=101351",
                          9.3,
                          876,
                          siemens,
                          Arrays.asList(Subject.CHEMIE, Subject.PHYSIK),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          Arrays.asList("Atom", "Verbindung", "Melokül", "Wasser", "Wasserstoff"),
                          Arrays.asList(7, 8, 9),
                          MaterialType.BILD,
                          License.CC_BY_SA,
                          Format.png);
        final Image landWasserErde =
                new Image(idLandWasserErde,
                          "Verteilung von Land und Wasser auf der Erde",
                          "70 % der gesamten Erdoberfläche sind von Ozeanen bedeckt. Die Landmasse beträgt also nur " +
                          "30 %.",
                          MEDIENPORTAL_SIEMENS +
                          ".php?objid=109650",
                          8.9,
                          126,
                          siemens,
                          Arrays.asList(Subject.CHEMIE, Subject.PHYSIK),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          Arrays.asList("Meer", "Wasser", "Land"),
                          Arrays.asList(5, 6, 7, 8, 9),
                          MaterialType.BILD,
                          License.CC_BY_SA,
                          Format.png);
        final Image image18 =
                new Image(idImage18,
                          "Energietraeger Wasser",
                          null,
                          MEDIENPORTAL_SIEMENS +
                          ".php?objid=100136",
                          9.1,
                          53,
                          bendover,
                          subjects18,
                          LocalDateTime.of(2019, 5, 13, 16, 18),
                          LocalDateTime.of(2019, 5, 13, 16, 18),
                          tags18,
                          gradeLevels18,
                          MaterialType.BILD,
                          License.CC_BY_SA,
                          Format.png);
        final Video marvVideoWahrscheinlichkeit =
                new Video(marvVideo1,
                          "Vorlesung 25: Gesetze der großen Zahlen",
                          null,
                          "https://olw-material.hrz.tu-darmstadt" +
                          ".de/olw-konv-repository/material/ba/28/97/30/42/a9/11/ea/ab/b6/00/50/56/bd/73/ae/9" +
                          ".mp4?filename=Vorlesung25GesetzeDerGrossenZahlen29022020.mp4",
                          4,
                          16,
                          userMarvin,
                          subjectMathe,
                          LocalDateTime.of(2020, 3, 5, 7, 0),
                          LocalDateTime.of(2020, 3, 5, 7, 0),
                          tagChance,
                          gradeChance,
                          82,
                          MaterialType.VIDEO,
                          License.CC_BY_SA,
                          Format.mp4);
        final Video video19 =
                new Video(idVideo19,
                          "Laufwasserkraftwerk",
                          null,
                          MEDIENPORTAL_SIEMENS +
                          ".php?objid=104837",
                          7.5,
                          71,
                          bendover,
                          subjects19,
                          LocalDateTime.of(2019, 5, 7, 12, 19),
                          LocalDateTime.of(2019, 5, 7, 12, 19),
                          tags19,
                          gradeLevels19,
                          1,
                          MaterialType.VIDEO,
                          License.CC_BY_SA,
                          Format.mp4);
        final Video verbrennen =
                new Video(idVerbrennen,
                          "Verbrennen macht schwerer oder leichter - was stimmt?",
                          "In einem Reagenzglas, das mit einem Luftballon abgeschlossen ist, befinden sich zwei" +
                          " Streichhölzer. Das Reagenzglas wird erhitzt. Was passiert?",
                          MEDIENPORTAL_SIEMENS +
                          ".php?objid=109197",
                          7.1,
                          110,
                          siemens,
                          Arrays.asList(Subject.CHEMIE),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          LocalDateTime.of(2020, 5, 14, 20, 40),
                          Arrays.asList("Chemische Reaktion", "Verbrennungsvorgang"),
                          Arrays.asList(5, 6, 7, 8, 9),
                          22,
                          MaterialType.VIDEO,
                          License.CC_BY_SA,
                          Format.mp4);
        final Video exponentialFunctionVideo =
                new Video(idVideoExpo,
                          "10.01 Exponentialfunktionen",
                          "In diesem Video werden Exponentialfunktionen erklärt.",
                          "https://j3l7h2.de/videos/Mathe_1_Teil_1,_Winter_2010_2011/10.01_Exponentialfunktionen.mp4",
                          10.0,
                          13,
                          maxMustermann,
                          List.of(Subject.MATHE),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          Arrays.asList("Funktion",
                                        "Exponentialfunktion",
                                        "Gleichung"),
                          Arrays.asList(10),
                          600,
                          MaterialType.VIDEO,
                          License.CC_BY_SA,
                          Format.mp4);
        final Video stringsInJavaVideo =
                new Video(idVideoJava,
                          "Strings in Java",
                          "In diesem Video werden Strings in Java erklärt.",
                          "https://olw-material.hrz.tu-darmstadt" +
                          ".de/olw-konv-repository/material/cc/f6/1a/e0/cc/f9/11/e2/a7/28/00/50/56/bd/73/ad/9" +
                          ".mp4?filename=StringsInJava.mp4",
                          9.1,
                          67,
                          maxMustermann,
                          List.of(Subject.INFORMATIK),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          LocalDateTime.of(2019, 5, 13, 16, 14),
                          Arrays.asList("Strings",
                                        "Java",
                                        "Programmierung"),
                          Arrays.asList(10),
                          1427,
                          MaterialType.VIDEO,
                          License.CC_BY_SA,
                          Format.mp4);
        final List<Media> wassermedia =
                Arrays.asList(wassermangel, wassermolekuehl, landWasserErde, verdunstungsvorgang);
        final Module wasser = new Module("Wasser", wassermedia, idWasserModule,
                                         LocalDateTime.of(2020, 5, 14, 20, 40),
                                         LocalDateTime.of(2020, 5, 14, 20, 40),
                                         userMarvin);

        LOGGER.info("Persisting with demo data");

        userMarvin.setFavourites(Arrays.asList(video19, text17));
        userMarvin.setContacts(Arrays.asList(bendover, siemens, maxMustermann));
        userMarvin.setMyModules(Arrays.asList(wasser));

        entityManager.persist(bendover);
        entityManager.persist(text16);
        entityManager.persist(text17);
        entityManager.persist(image18);
        entityManager.persist(video19);
        entityManager.persist(text20);
        entityManager.persist(maxMustermann);
        entityManager.persist(classFilter);
        entityManager.persist(dateFilter);
        entityManager.persist(formatFilter);
        entityManager.persist(subjectFilter);
        entityManager.persist(materialTypeFilter);
        entityManager.persist(idFilter);
        entityManager.persist(search);
        entityManager.persist(search2);
        entityManager.persist(search3);
        entityManager.persist(search4);
        entityManager.persist(siemens);
        entityManager.persist(userMarvin);
        entityManager.persist(marvTextTrigonometrie);
        entityManager.persist(marvTextKomplexeZahlen);
        entityManager.persist(marvTextGrundwissenMathematik);
        entityManager.persist(marvTextSolarzelle);
        entityManager.persist(marvVideoWahrscheinlichkeit);
        entityManager.persist(wassermolekuehl);
        entityManager.persist(wassermangel);
        entityManager.persist(landWasserErde);
        entityManager.persist(verdunstungsvorgang);
        entityManager.persist(verbrennen);
        entityManager.persist(wasser);
        entityManager.persist(photosynthesisText);
        entityManager.persist(eyeImage);
        entityManager.persist(earImage);
        entityManager.persist(exponentialFunctionVideo);
        entityManager.persist(stringsInJavaVideo);

        messageFactory.getConversions(3).forEach(conversation -> Arrays.stream(conversation)
                                                                       .map(messageData -> new Message(UUID.randomUUID(),
                                                                                                       messageData.sender,
                                                                                                       messageData.receiver,
                                                                                                       messageData.message,
                                                                                                       messageData.dateSent))
                                                                       .forEach(message -> entityManager.persist(message)));
    }
}
