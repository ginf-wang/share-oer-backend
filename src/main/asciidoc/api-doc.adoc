= API Dokumentation *backend.bm4oer.de*
Projekt BM4OER <bm4oer@uni-bremen.de>
v1.5, 2020-05-23
:toc: left
:toc-title: Inhalt
:toclevels: 4
:numbered:
:sectanchors:

Diese Dokumentation erklärt alle verfügbaren Ressourcen des Backends.

== Meta

Unabhängig vom Datenmodell bestehende Ressourcen und generelle Informationen.

=== Demodaten

Demodaten werden alle 30min neu eingespielt.
Die Zeit bis zum nächsten Reset ist unter der <<info>> ersichtlich.

=== Authentifikation

Die Authentifikation geschieht per https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic auth].

Erforderlich ist dies auf den Ressourcen `/user`, `/search`, `/filter` und `/message` sowie beim Anlegen jeglicher Medien.

Derzeit verfügbare User:

|===
|User|Passwort

|marv@bm4oer.de
|Schwertfisch
|===

=== Enumerations

Folgende Arrays haben eine spezifische Wertebelegung (vgl. dazu die Repräsentation unter <<medium>>), welche als String mitgegeben werden muss.

Die jeweiligen Werte lassen sich auch über die Resourcen `/format`, `/license`, `/materialtype` und `/subject` abrufen.

==== Dateiformat - _Format_

[#format]
.Format
[%collapsible]
====
operation::enum_tests/successful_get_format_request/1[snippets='http-request,http-response']
====

==== Lizenz - _License_

.License
[%collapsible]
====
operation::enum_tests/successful_get_license_request/1[snippets='http-request,http-response']
====

==== Art des Materials - _MaterialType_

[#materialtype]
.MaterialType
[%collapsible]
====
operation::enum_tests/successful_get_material_type_request/1[snippets='http-request,http-response']
====

==== Schulfach - _Subject_

[#subject]
.Subject
[%collapsible]
====
operation::enum_tests/successful_get_subject_request/1[snippets='http-request,http-response']
====

==== Sortierung - _Sort_

[#sort]
.Sort
[%collapsible]
====
operation::enum_tests/successful_get_sort_request/1[snippets='http-request,http-response']
====

[#info]
=== Info-Ressource

Die `/info`-Ressource bietet nützliche Informationen in Bezug auf die Revision und das Zurücksetzen der Demodaten.

.Details
[%collapsible]
====

operation::info_resource_tests/successful_request/1[snippets='http-request,http-response']

====

== Medien

Medien sind einzelne Lehrmittel, welche einerseits als Metadaten erfasst und andererseits als Nutzdaten gespeichert werden.

=== _Media_

Per `/media` werden alle Medientypen gleichermaßen abgebildet, möglich ist hier nur Abrufen und Löschen per _ID_.

==== Ersten Zehn Medien abrufen

.Details
[%collapsible]
====
operation::media_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Zweiten Zehn Medien abrufen

Um die zweiten Zehn Medien abzurufen muss der Request ein Parameter id mitgegeben werden.
Dieser Parameter ist die Id des letzten Mediums der vorherigen Zehn Medien.

.Details
[%collapsible]
====
operation::media_resource_tests/successful_get_all_request_with_cursor/1[snippets='http-request,http-response']
====

[#medium]
==== Bestimmte Anzahl an Medien abrufen

Um die eine bestimmte Anzahl an Medien abzurufen muss der Request ein Parameter results mitgegeben werden.
Dieser Parameter bestimmt die Anzahl an Ergebnissen.

.Details
[%collapsible]
====
operation::media_resource_tests/successful_get_all_request_with_results/1[snippets='http-request,http-response']
====

==== Ein Medium herunterladen

.Details
[%collapsible]
====
operation::media_resource_tests/successful_get_by_i_d_video_request/1[snippets='http-request']
====

==== Unbestimmtes Medium löschen

.Details
[%collapsible]
====
operation::media_resource_tests/successful_image_delete_request/1[snippets='http-request,http-response']
====

=== _Text_

`/text` dient der Abbildung von Texten als Medientypen.

==== Alle Texte abrufen

.Details
[%collapsible]
====
operation::text_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Einen bestimmten Text abrufen

.Details
[%collapsible]
====
operation::id_filter_tests/successful_get_text_by_filter/1[snippets='http-request,http-response']
====

==== Einen Text herunterladen

.Details
[%collapsible]
====
operation::text_resource_tests/successful_get_by_i_d_request/1[snippets='http-request']
====

==== Einen Text anlegen

.Details
[%collapsible]
====
operation::text_resource_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Einen Text verändern

.Details
[%collapsible]
====
operation::text_resource_tests/successful_update_request/1[snippets='http-request,http-response']
====

==== Einen Text löschen

.Details
[%collapsible]
====
operation::text_resource_tests/successful_delete_request/1[snippets='http-request,http-response']
====

=== _Image_

`/image` dient der Abbildung von Bildern als Medientypen.

==== Alle Bilder abrufen

.Details
[%collapsible]
====
operation::image_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Ein bestimmtes Bild abrufen

.Details
[%collapsible]
====
operation::id_filter_tests/successful_get_image_by_filter/1[snippets='http-request,http-response']
====

==== Ein Bild herunterladen

.Details
[%collapsible]
====
operation::image_resource_tests/successful_get_by_i_d_request/1[snippets='http-request']
====

==== Ein Bild anlegen

.Details
[%collapsible]
====
operation::image_resource_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Ein Bild verändern

.Details
[%collapsible]
====
operation::image_resource_tests/successful_update_request/1[snippets='http-request,http-response']
====

==== Ein Bild löschen

.Details
[%collapsible]
====
operation::image_resource_tests/successful_delete_request/1[snippets='http-request,http-response']
====

=== _Video_

`/video` dient der Abbildung von Videos als Medientypen.

==== Alle Videos abrufen

.Details
[%collapsible]
====
operation::video_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Ein bestimmtes Video abrufen

.Details
[%collapsible]
====
operation::id_filter_tests/successful_get_video_by_filter/1[snippets='http-request,http-response']
====

==== Ein Video herunterladen

.Details
[%collapsible]
====
operation::video_resource_tests/successful_get_by_i_d_request/1[snippets='http-request']
====

==== Ein Video anlegen

.Details
[%collapsible]
====
operation::video_resource_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Ein Video verändern

.Details
[%collapsible]
====
operation::video_resource_tests/successful_update_request/1[snippets='http-request,http-response']
====

==== Ein Video löschen

.Details
[%collapsible]
====
operation::video_resource_tests/successful_delete_request/1[snippets='http-request,http-response']
====

=== _Module_

Module entsprechen Mediensammlungen, die von Nutzern erstellt werden können.
Nutzbar sind sie über die `/module`-Ressource.

==== Alle Module abrufen

.Details
[%collapsible]
====
operation::module_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Ein bestimmtes Modul abrufen

.Details
[%collapsible]
====
operation::module_resource_tests/successful_get_by_i_d_request/1[snippets='http-request,http-response']
====

==== Ein Modul anlegen

.Details
[%collapsible]
====
operation::module_resource_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Ein Modul verändern

.Details
[%collapsible]
====
operation::module_resource_tests/successful_update_request/1[snippets='http-request,http-response']
====

==== Ein Modul löschen

.Details
[%collapsible]
====
operation::module_resource_tests/successful_delete_request/1[snippets='http-request,http-response']
====

== Suche

Eine Suche besteht aus einem Suchbegriff und einer optionalen Sammlung von Filtern, welche weitere Konditionen setzen.
Zusätzlich lässt sich optional die Sortierreihenfolge der Suche mithilfe der <<sort,Sortierwerte>> bestimmen.

=== Suche absetzen

Um die Suchfunktion zu verwenden muss zuerst eine Suche mit mindestens einem Filter oder einem Suchbegriff
<<Eine Suche mit Filtern persistent anlegen,persistiert>> werden.
Die Id dieser Suche muss nun beim <<medium,Aufrufen aller Medien>> als Parameter searchId übergeben werden.

.Details
[%collapsible]
====
operation::media_resource_tests/successful_search_execution/1[snippets='http-request,http-response']
====

=== _Search_

Um als Nutzer Suchaufträge zu speichern, steht die `/search`-Ressource zur Verfügung.

==== Eine Suche persistent anlegen

.Details
[%collapsible]
====
operation::search_resource_test/successful_post_request/1[snippets='http-request,http-response']
====

==== Eine gespeicherte Suche abfragen

.Details
[%collapsible]
====
operation::search_resource_test/successful_get_by_i_d_request/1[snippets='http-request,http-response']
====

==== Eine gespeicherte Suche entfernen

.Details
[%collapsible]
====
operation::search_resource_test/successful_delete_request/1[snippets='http-request,http-response']
====

=== _Filter_

Konkrete Filter werden je nach Typ entweder in einer Ausprägung oder als Von-Bis verwendet.
Sie stehen jeweils als Subresource von `/filter` bereit:

|===
|Ressource |Filterklasse |Werte

|`/date`
|Zeitraum
|_Date_ fromDate +
_Date_ toDate

|`/class`
|Klassenstufe
|_Int_ fromClass +
_Int_ toClass

|`/format`
|Dateiformat +
_siehe <<format>>_
|_String_ fileFormat

|`/material`
|Medientyp +
_siehe <<materialtype>>_
|_String_ materialtype

|`/subject`
|Schulfach +
_siehe <<subject>>_
|_String[]_ subjects

|`/id`
|Konkretes Objekt
|_String_ id
|===

== Benutzer

Nutzer der Plattform besitzen eigene Profildaten, Communityfunktionen und Speichermöglichkeiten für Suchen.

=== _User_

Unter `/user` lässt sich mit der Benutzerverwaltung interagieren.

==== Alle Nutzer abrufen

.Details
[%collapsible]
====
operation::user_resource_tests/successful_get_all_request/1[snippets='http-request,http-response']
====

==== Einen bestimmten Nutzer abrufen

.Details
[%collapsible]
====
operation::user_resource_tests/successful_get_by_i_d_request/1[snippets='http-request,http-response']
====

==== Einen Nutzer anlegen

.Details
[%collapsible]
====
operation::user_resource_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Einen Nutzer verändern

.Details
[%collapsible]
====
operation::user_resource_tests/successful_update_request/1[snippets='http-request,http-response']
====

==== Einen Nutzer entfernen

.Details
[%collapsible]
====
operation::user_resource_tests/successful_delete_request/1[snippets='http-request,http-response']
====

=== _Message_

==== Eine Nachricht senden

.Details
[%collapsible]
====
operation::message_tests/successful_post_request/1[snippets='http-request,http-response']
====

==== Alle Nachrichten abrufen

.Details
[%collapsible]
====
operation::message_tests/successful_get_all_request/1[snippets='http-request,http-response']
====
