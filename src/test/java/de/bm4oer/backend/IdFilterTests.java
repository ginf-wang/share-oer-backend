package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.IdFilter;
import de.bm4oer.backend.model.Search;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests id filter resources
 *
 * @author decker1@uni-bremen.de
 */
class IdFilterTests extends TestImpl {

    private final String urlTemplateSearch = "/search/";
    private final String urlTemplateMedia = "/media/";

    private final String idSearchText = "401fde26-bdfa-47ce-a8da-fc5d6a20a605";
    private final String idSearchImage = "97912b97-f5aa-4753-8b0b-e490cd4230e8";
    private final String idSearchVideo = "00161947-900e-4292-93a8-dd2f0869c6c7";

    private final IdFilter idfilter = new IdFilter(null,
                                                   UUID.fromString("958270b7-7018-4c46-a263-5897b647edfa"));

    private final Search search = new Search(null, "", Arrays.asList(idfilter), null);

    private final ObjectMapper om = new ObjectMapper();

    private final String searchJSON = om.writeValueAsString(search);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    IdFilterTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test id filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplateSearch)
                        .content(searchJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test text id filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetTextByFilter() throws Exception {
        getMockMvc().perform(
                get(urlTemplateMedia)
                        .param("searchId", idSearchText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test video id filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetVideoByFilter() throws Exception {
        getMockMvc().perform(
                get(urlTemplateMedia)
                        .param("searchId", idSearchVideo)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test image id filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulGetImageByFilter() throws Exception {
        getMockMvc().perform(
                get(urlTemplateMedia)
                        .param("searchId", idSearchImage)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(status().isOk());
    }
}
