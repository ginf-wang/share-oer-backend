package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.User;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests user resources
 *
 * @author hama@uni-bremen.de
 */
class UserResourceTests extends TestImpl {

    private final String urlTemplate = "/user";

    private final User max =
            new User("max@test.test",
                     "max",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null
            );

    private final User merle =
            new User("merle@test.test",
                     "merle",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null
            );

    private final ObjectMapper om = new ObjectMapper();

    private final String maxJSON = om.writeValueAsString(max);
    private final String merleJSON = om.writeValueAsString(merle);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    UserResourceTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test user has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(merleJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("email").value(merle.getEmail()))
                    .andExpect(jsonPath("username").value(merle.getUsername()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test users have been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a put request and inspects the response regarding its content type and HTTP response code.
     * Afterwards, a get request is being performed to make sure that the content has been properly updated.
     * The tests is successful when the content has been successfully updated.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulUpdateRequest() throws Exception {
        getMockMvc().perform(
                put(urlTemplate + "/ben@example.com")
                        .content(maxJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());

        getMockMvc().perform(
                get(urlTemplate + "/max@test.test")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("email").value(max.getEmail()))
                    .andExpect(jsonPath("username").value(max.getUsername()))
                    .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test user has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + "/ben@example.com")
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("email").value("ben@example.com"))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test user has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + "/ben@example.com")
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("email").value("ben@example.com"))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test user has not been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(6)
    void notFoundError() throws Exception {
        getMockMvc().perform(
                get("/merle@test.test")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }
}
