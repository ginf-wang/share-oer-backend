package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.EnumSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests enum resources
 *
 * @author iapachi@uni-bremen.de
 */
public class EnumTests extends TestImpl {

    private final String urlTemplateSubject = "/enums/subject/";
    private final String urlTemplateMaterialType = "/enums/materialtype/";
    private final String urlTemplateLicense = "/enums/license/";
    private final String urlTemplateFormat = "/enums/format/";
    private final String urlTemplateSort = "/enums/sort/";

    private final ObjectMapper om = new ObjectMapper();

    private final String expectedSubject = om.writeValueAsString(EnumSet.allOf(Subject.class));
    private final String expectedMaterialType = om.writeValueAsString(EnumSet.allOf(MaterialType.class));
    private final String expectedLicense = om.writeValueAsString(EnumSet.allOf(License.class));
    private final String expectedFormat = om.writeValueAsString(EnumSet.allOf(Format.class));
    private final String expectedSort = om.writeValueAsString(EnumSet.allOf(Sort.class));

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    EnumTests() throws JsonProcessingException {}

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the format enum has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulGetFormatRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplateFormat)
                        .contentType(MediaType.TEXT_XML)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().string(expectedFormat))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the license enum has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetLicenseRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplateLicense)
                        .contentType(MediaType.TEXT_XML)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().string(expectedLicense))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the material enum has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetMaterialTypeRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplateMaterialType)
                        .contentType(MediaType.TEXT_XML)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().string(expectedMaterialType))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the subject enum has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulGetSubjectRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplateSubject)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().string(expectedSubject))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the sort enum has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulGetSortRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplateSort)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(content().string(expectedSort))
                    .andExpect(status().isOk());
    }
}
