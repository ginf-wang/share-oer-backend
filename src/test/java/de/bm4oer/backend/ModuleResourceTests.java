package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.Module;
import de.bm4oer.backend.model.User;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests module resources
 *
 * @author iapachi@uni-bremen.de
 */
class ModuleResourceTests extends TestImpl {

    private final String urlTemplate = "/module/";

    private final User testUser =
            new User("siemens@example.com",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null);

    private final Module testModule =
            new Module("testModule",
                       null,
                       null,
                       null,
                       null,
                       testUser
            );

    private final Module updatedTestModule =
            new Module("updatedTestModule",
                       null,
                       null,
                       null,
                       null,
                       testUser
            );

    private final String id = "7dd12cb3-38cd-40ce-82f6-099a37665b22";

    private final ObjectMapper om = new ObjectMapper();

    private final String testModuleJSON = om.writeValueAsString(testModule);
    private final String updatedTestModuleJSON = om.writeValueAsString(updatedTestModule);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    ModuleResourceTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test module has been successfully created.
     * User authentication is implicitly required for this test to work.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(testModuleJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("name").value(testModule.getName()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test module has been successfully received.
     * User authentication is implicitly required for this test to work.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("name").value("Wasser"))
                    .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the HTTP response code is OK.
     * User authentication is implicitly required for this test to work.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a put request and inspects the response regarding its content type and HTTP response code.
     * Afterwards, a get request is being performed to make sure that the content has been properly updated.
     * The tests is successful when the content has been successfully updated.
     * User authentication is implicitly required for this test to work.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulUpdateRequest() throws Exception {
        getMockMvc().perform(
                put(urlTemplate + id)
                        .content(updatedTestModuleJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());

        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("name").value(updatedTestModule.getName()))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the content has been successfully deleted.
     * User authentication is implicitly required for this test to work.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + id)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(id))
                    .andExpect(status().isOk());
    }
}
