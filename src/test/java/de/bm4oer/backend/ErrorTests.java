package de.bm4oer.backend;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests error resources
 *
 * @author jlk@uni-bremen.de
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class ErrorTests {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the HTTP response code is unauthorized.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    void successfulNotAuthorized() throws Exception {
        mockMvc.perform(
                get("/user")
        )
               .andExpect(status().isUnauthorized())
               .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the HTTP response code is not found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @WithMockUser(username = BackendApplication.ApplicationSecurityConfig.username,
                  password = BackendApplication.ApplicationSecurityConfig.password)
    void notFoundError() throws Exception {
        mockMvc.perform(
                get("/wont-be-found")
        )
               .andExpect(status().isNotFound())
               .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the status is 999, there is no error and a message with a given timestamp exists.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    void successfulErrorRequest() throws Exception {
        mockMvc.perform(
                get("/error")
        )
               .andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("status").value(999))
               .andExpect(jsonPath("error").value("None"))
               .andExpect(jsonPath("message").exists())
               .andExpect(jsonPath("timestamp").exists())
               .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the HTTP response code is bad request.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @WithMockUser(username = BackendApplication.ApplicationSecurityConfig.username,
                  password = BackendApplication.ApplicationSecurityConfig.password)
    void badRequestError() throws Exception {
        mockMvc.perform(
                get("/module/bad-id")
        )
               .andExpect(status().isBadRequest())
               .andDo(print());
    }
}
