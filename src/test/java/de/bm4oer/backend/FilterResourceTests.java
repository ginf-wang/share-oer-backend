package de.bm4oer.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests filter resources
 *
 * @author hama@uni-bremen.de
 */
class FilterResourceTests extends TestImpl {

    private final String urlTemplate = "/filter";
    private final String urlClass = "/class/29fab9a6-ec93-4bc2-84af-22f044d00dad";
    private final String urlDate = "/date/ae516f13-3569-4151-b22f-a1aa54459a13";
    private final String urlFormat = "/format/c33109ae-f579-40ad-8e99-a8af5b040039";
    private final String urlMaterialType = "/materialtype/7fd8a636-9046-4c0a-a67d-44765d8a12f3";
    private final String urlSubjects = "/subject/499ddebc-c932-43cd-8ffb-ef06d6d906c0";
    private final ClassFilter classFilter = new ClassFilter(7, 9,
                                                            UUID.randomUUID());
    private final DateFilter dateFilter = new DateFilter(LocalDate.of(2019, 5, 1), LocalDate.of(2020, 5, 31),
                                                         UUID.randomUUID());
    private final FormatFilter formatFilter = new FormatFilter(Arrays.asList(Format.pdf),
                                                               UUID.randomUUID());
    private final SubjectFilter subjectFilter =
            new SubjectFilter(UUID.randomUUID(),
                              Subject.BIOLOGIE);
    private final MaterialTypeFilter materialTypeFilter =
            new MaterialTypeFilter(Arrays.asList(MaterialType.ZUSAMMENFASSUNG),
                                   UUID.randomUUID());

    private final ObjectMapper om = new ObjectMapper();

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test class filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulClassPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate + "/class")
                        .content(om.writeValueAsBytes(classFilter))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test class filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetClassByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + urlClass)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("fromClass").value(7))
                    .andExpect(jsonPath("toClass").value(9))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test class filter has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulClassDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + urlClass)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test class id hasn't been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void notFoundErrorClass() throws Exception {
        getMockMvc().perform(
                get("/class/60c186bf-ddc5-4b06-8b37-02e203f8ad12")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test date filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDatePostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate + "/date")
                        .content(om.writeValueAsBytes(dateFilter))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test date filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(6)
    void successfulDateGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + urlDate)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("fromDate").value("2019-05-01"))
                    .andExpect(jsonPath("toDate").value("2020-05-31"))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test date filter has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(7)
    void successfulDateDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + urlDate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test date id hasn't been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(8)
    void notFoundErrorDate() throws Exception {
        getMockMvc().perform(
                get("/date/60c186bf-ddc5-4b06-8b37-02e203f8ad12")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test format filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(9)
    void successfulFormatPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate + "/format")
                        .content(om.writeValueAsBytes(formatFilter))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test format filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(10)
    void successfulFormatGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + urlFormat)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("fileFormat").value("pdf"))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test format filter has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(11)
    void successfulFormatDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + urlFormat)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test format id hasn't been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(12)
    void notFoundErrorFormat() throws Exception {
        getMockMvc().perform(
                get("/format/60c186bf-ddc5-4b06-8b37-02e203f8ad12")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test material filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(13)
    void successfulMaterialTypePostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate + "/materialtype")
                        .content(om.writeValueAsBytes(materialTypeFilter))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test material filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(14)
    void successfulMaterialTypeGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + urlMaterialType)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("materialType").value(MaterialType.ZUSAMMENFASSUNG.toString()))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test material filter has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(15)
    void successfulMaterialTypeDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + urlMaterialType)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test material id hasn't been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(16)
    void notFoundErrorFormatMaterialType() throws Exception {
        getMockMvc().perform(
                get("/materialtype/60c186bf-ddc5-4b06-8b37-02e203f8ad12")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }

    /**
     * Performs a post request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test subject filter has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(17)
    void successfulSubjectPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate + "/subject")
                        .content(om.writeValueAsBytes(subjectFilter))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test subject filter has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(18)
    void successfulSubjectGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + urlSubjects)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("subjects").value("BIOLOGIE"))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test subject filter has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(19)
    void successfulSubjectTypeDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + urlSubjects)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test subject id hasn't been found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(20)
    void notFoundErrorFormatSubject() throws Exception {
        getMockMvc().perform(
                get("/subject/60c186bf-ddc5-4b06-8b37-02e203f8ad12")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }
}
