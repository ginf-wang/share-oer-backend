package de.bm4oer.backend;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests the info resource
 *
 * @author jlk@uni-bremen.de
 */
class InfoResourceTests extends TestImpl {

    private final String urlTemplate = "/info";

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the response includes (the latest) commit, build time and database reset.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    void successfulRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("commit").exists())
                    .andExpect(jsonPath("buildTime").exists())
                    .andExpect(jsonPath("databaseReset").exists())
                    .andDo(print());
    }
}
