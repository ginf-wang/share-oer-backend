package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests search resources
 *
 * @author hama@uni-bremen.de
 */
class SearchResourceTest extends TestImpl {
    private final String urlTemplate = "/search/";

    private final String idSearch = "ae7ada97-b296-4d21-adfc-8331b45d42fb";

    private final ClassFilter classFilter = new ClassFilter(7, 9,
                                                            null);
    private final DateFilter dateFilter = new DateFilter(LocalDate.of(2017, 5, 1),
                                                         LocalDate.of(2020, 5, 31),
                                                         null);
    private final FormatFilter formatFilter = new FormatFilter(Arrays.asList(Format.pdf),
                                                               null);
    private final SubjectFilter subjectFilter = new SubjectFilter(null, Subject.BIOLOGIE);
    private final MaterialTypeFilter materialTypeFilter =
            new MaterialTypeFilter(Arrays.asList(MaterialType.ZUSAMMENFASSUNG), null);

    private final List<Filter> filters =
            Arrays.asList(classFilter, dateFilter, subjectFilter, formatFilter, materialTypeFilter);

    private final Search search = new Search(null, "Photosynthese", filters, null);
    private final Search searchAsc = new Search(null, "", Collections.singletonList(dateFilter), Sort.DATE_ASC);
    private final Search searchDesc = new Search(null, "", Collections.singletonList(formatFilter), Sort.RATING_DESC);

    private final ObjectMapper om = new ObjectMapper();

    private final String searchJSON = om.writeValueAsString(search);
    private final String searchAscJSON = om.writeValueAsString(searchAsc);
    private final String searchDescJSON = om.writeValueAsString(searchDesc);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    SearchResourceTest() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test module has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        System.out.println("searchJSON = " + searchJSON);
        getMockMvc().perform(
                post(urlTemplate)
                        .content(searchJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("searchTerm").value(search.getSearchTerm()))
                    .andExpect(jsonPath("filters").isArray())
                    .andExpect(jsonPath("sort").value(search.getSort()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test search has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + idSearch)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
        )
                    .andExpect(status().isOk())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(idSearch))
                    .andExpect(jsonPath("searchTerm").value("Photosynthese"))
                    .andExpect(jsonPath("filters").isArray())
                    .andExpect(jsonPath("sort").value(Sort.RATING_DESC.toString()))
                    .andDo(print());
    }

    /**
     * Performs a delete request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test search has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + idSearch)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test search (asc) has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulPostSortAscRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(searchAscJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("searchTerm").value(searchAsc.getSearchTerm()))
                    .andExpect(jsonPath("filters").isArray())
                    .andExpect(jsonPath("sort").value(searchAsc.getSort().toString()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test search (desc) has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(6)
    void successfulPostSortDescRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(searchDescJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("searchTerm").value(searchDesc.getSearchTerm()))
                    .andExpect(jsonPath("filters").isArray())
                    .andExpect(jsonPath("sort").value(searchDesc.getSort().toString()))
                    .andExpect(status().isOk());
    }
}

