package de.bm4oer.backend;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests media resources
 *
 * @author iapachi@uni-bremen.de
 */
class MediaResourceTests extends TestImpl {
    private final String urlTemplate = "/media/";

    private final String idText = "958270b7-7018-4c46-a263-5897b647edfa";
    private final String idVideo = "ae7ada97-e596-4d21-adfc-8331b45d42ca";
    private final String idImage = "ae7ada97-d496-4d21-adfc-8331b45d42ba";

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test media has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulGetByIDTextRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + idText)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test media has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDVideoRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + idVideo)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test media has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetByIDImageRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + idImage)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test medias have been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test medias have been successfully received with a cursor.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulGetAllRequestWithCursor() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + "?id=61b05f64-0786-4851-9e1f-7f0b10fd5f3b")
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test medias have been successfully received with (5) results.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulGetAllRequestWithResults() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + "?results=5")
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test media has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(6)
    void successfulTextDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + idText)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(idText))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test media has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(7)
    void successfulVideoDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + idVideo)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(idVideo))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test media has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(8)
    void successfulImageDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + idImage)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(idImage))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test search has been successfully performed.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(9)
    void successfulSearchExecution() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
                        .param("searchId", "ae7ada97-b296-4d21-adfc-8331b45d42fb")
                        .characterEncoding("UTF-8")
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }
}
