package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests video resources
 *
 * @author iapachi@uni-bremen.de
 */
class VideoResourceTests extends TestImpl {
    private final String urlTemplate = "/video/";

    private final String id = "ae7ada97-e596-4d21-adfc-8331b45d42ca";

    private final List<Subject> subjects = List.of(Subject.BIOLOGIE);
    private final List<String> tags = Arrays.asList("Auge", "Anatomie", "Humanbiologie");
    private final List<Integer> gradeLevels = Arrays.asList(5, 6, 7, 8);

    private final User maxMustermann =
            new User("max_mustermann@example.com",
                     "max_mustermann",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null
            );

    private final Video video = new Video(null,
                                          "TestVideo",
                                          null,
                                          "test.mp4",
                                          0,
                                          0,
                                          maxMustermann,
                                          subjects,
                                          null,
                                          null,
                                          tags,
                                          gradeLevels,
                                          5,
                                          MaterialType.VIDEO,
                                          License.CC_BY,
                                          Format.mp4);

    private final Video updatedVideo = new Video(null,
                                                 "TestVideoUpdated",
                                                 null,
                                                 "https://medienportal.siemens-stiftung.org/lib/obj_view" +
                                                 ".php?objid=104837&sessid=4dc074a05f3140b7f91194a9af6345d1",
                                                 0,
                                                 0,
                                                 maxMustermann,
                                                 subjects,
                                                 null,
                                                 null,
                                                 tags,
                                                 gradeLevels,
                                                 5,
                                                 MaterialType.SONSTIGES,
                                                 License.CC_BY_NC,
                                                 Format.txt);

    private final ObjectMapper om = new ObjectMapper();

    private final String videoJSON = om.writeValueAsString(video);
    private final String updatedVideoJSON = om.writeValueAsString(updatedVideo);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    VideoResourceTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test video has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(videoJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(video.getTitle()))
                    .andExpect(jsonPath("fileName").value(video.getFileName()))
                    .andExpect(jsonPath("owner.email").value(maxMustermann.getEmail()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test video has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test videos have been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a put request and inspects the response regarding its content type and HTTP response code.
     * Afterwards, a get request is being performed to make sure that the content has been properly updated.
     * The tests is successful when the content has been successfully updated.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulUpdateRequest() throws Exception {
        getMockMvc().perform(
                put(urlTemplate + id)
                        .content(updatedVideoJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(updatedVideo.getTitle()))
                    .andExpect(jsonPath("fileName").value(updatedVideo.getFileName()))
                    .andExpect(jsonPath("materialType").value(updatedVideo.getMaterialType().toString()))
                    .andExpect(jsonPath("license").value(updatedVideo.getLicense().toString()))
                    .andExpect(jsonPath("format").value(updatedVideo.getFormat().toString()))
                    .andExpect(status().isOk());

        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test video has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + id)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(id))
                    .andExpect(status().isOk());
    }
}
