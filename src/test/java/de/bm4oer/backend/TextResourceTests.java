package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests text resources
 *
 * @author iapachi@uni-bremen.de
 */
class TextResourceTests extends TestImpl {
    private final String urlTemplate = "/text/";
    private final String id = "958270b7-7018-4c46-a263-5897b647edfa";

    private final List<Subject> subjects = List.of(Subject.BIOLOGIE);
    private final List<String> tags = Arrays.asList("Auge", "Anatomie", "Humanbiologie");
    private final List<Integer> gradeLevels = Arrays.asList(5, 6, 7, 8);

    private final User testUser =
            new User("siemens@example.com",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null);

    private final Text text =
            new Text(null,
                     "TestText",
                     null,
                     "test.txt",
                     0,
                     0,
                     testUser,
                     subjects,
                     null,
                     null,
                     tags,
                     gradeLevels,
                     12,
                     MaterialType.ARBEITSBLATT,
                     License.CC_BY,
                     Format.pdf);

    private final Text updatedText =
            new Text(null,
                     "TestTextUpdated",
                     null,
                     "https://www.grund-wissen.de/mathematik/_downloads/grundwissen-mathematik.pdf",
                     0,
                     0,
                     testUser,
                     subjects,
                     null,
                     null,
                     tags,
                     gradeLevels,
                     2,
                     MaterialType.VIDEO,
                     License.CC_BY_NC,
                     Format.png);

    private final ObjectMapper om = new ObjectMapper();

    private final String textJSON = om.writeValueAsString(text);
    private final String updatedTextJSON = om.writeValueAsString(updatedText);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    TextResourceTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test text has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(textJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(text.getTitle()))
                    .andExpect(jsonPath("fileName").value(text.getFileName()))
                    .andExpect(jsonPath("owner.email").value(testUser.getEmail()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test text has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test texts have been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a put request and inspects the response regarding its content type and HTTP response code.
     * Afterwards, a get request is being performed to make sure that the content has been properly updated.
     * The tests is successful when the content has been successfully updated.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulUpdateRequest() throws Exception {
        getMockMvc().perform(
                put(urlTemplate + id)
                        .content(updatedTextJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(updatedText.getTitle()))
                    .andExpect(jsonPath("fileName").value(updatedText.getFileName()))
                    .andExpect(jsonPath("materialType").value(updatedText.getMaterialType().toString()))
                    .andExpect(jsonPath("license").value(updatedText.getLicense().toString()))
                    .andExpect(jsonPath("format").value(updatedText.getFormat().toString()))
                    .andExpect(status().isOk());

        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test text has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + id)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(id))
                    .andExpect(status().isOk());
    }
}
