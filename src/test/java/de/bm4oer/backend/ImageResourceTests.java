package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests image resources
 *
 * @author iapachi@uni-bremen.de
 */
class ImageResourceTests extends TestImpl {
    private final String urlTemplate = "/image/";

    private final List<Subject> subjects = List.of(Subject.BIOLOGIE);
    private final List<String> tags = Arrays.asList("Auge", "Anatomie", "Humanbiologie");
    private final List<Integer> gradeLevels = Arrays.asList(5, 6, 7, 8);

    private final User testUser =
            new User("siemens@example.com",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null);

    private final Image image =
            new Image(null,
                      "TestImage",
                      null,
                      "test.png",
                      0,
                      0,
                      testUser,
                      subjects,
                      null,
                      null,
                      tags,
                      gradeLevels,
                      MaterialType.BILD,
                      License.CC_BY,
                      Format.png);

    private final Image updatedImage =
            new Image(null,
                      "TestImageUpdated",
                      null,
                      "https://medienportal.siemens-stiftung.org/lib/obj_view" +
                      ".php?objid=100136&sessid=4dc074a05f3140b7f91194a9af6345d1",
                      0,
                      0,
                      testUser,
                      subjects,
                      null,
                      null,
                      tags,
                      gradeLevels,
                      MaterialType.VORLESUNG,
                      License.CC_BY_NC,
                      Format.jpg);

    private final String id = "ae7ada97-d496-4d21-adfc-8331b45d42ba";

    private final ObjectMapper om = new ObjectMapper();

    private final String imageJSON = om.writeValueAsString(image);
    private final String updatedImageJSON = om.writeValueAsString(updatedImage);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    ImageResourceTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test image has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(urlTemplate)
                        .content(imageJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(image.getTitle()))
                    .andExpect(jsonPath("fileName").value(image.getFileName()))
                    .andExpect(jsonPath("owner.email").value(testUser.getEmail()))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the test image has been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetByIDRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the test images have been successfully received.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(urlTemplate)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk());
    }

    /**
     * Performs a put request and inspects the response regarding its content type and HTTP response code.
     * Afterwards, a get request is being performed to make sure that the content has been properly updated.
     * The tests is successful when the content has been successfully updated.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(4)
    void successfulUpdateRequest() throws Exception {
        getMockMvc().perform(
                put(urlTemplate + id)
                        .content(updatedImageJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("title").value(updatedImage.getTitle()))
                    .andExpect(jsonPath("fileName").value(updatedImage.getFileName()))
                    .andExpect(jsonPath("materialType").value(updatedImage.getMaterialType().toString()))
                    .andExpect(jsonPath("license").value(updatedImage.getLicense().toString()))
                    .andExpect(jsonPath("format").value(updatedImage.getFormat().toString()))
                    .andExpect(status().isOk());

        getMockMvc().perform(
                get(urlTemplate + id)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andExpect(status().isSeeOther());
    }

    /**
     * Performs a delete request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test image has been successfully deleted.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(5)
    void successfulDeleteRequest() throws Exception {
        getMockMvc().perform(
                delete(urlTemplate + id)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").value(id))
                    .andExpect(status().isOk());
    }
}
