package de.bm4oer.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.bm4oer.backend.model.Message;
import de.bm4oer.backend.model.User;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests message resources
 *
 * @author jlk@uni-bremen.de
 */
public class MessageTests extends TestImpl {

    private final String url = "/message";

    private final User sender =
            new User("siemens@example.com",
                     "siemens",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null);

    private final User receiver =
            new User("max_mustermann@example.com",
                     "max_mustermann",
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null);

    private final Message testMessage =
            new Message(null, receiver, sender, "Das ist ein Test", null);

    private final ObjectMapper om = new ObjectMapper();

    private final String messageJSON = om.writeValueAsString(testMessage);

    /**
     * @throws JsonProcessingException when JSON cannot be created.
     */
    public MessageTests() throws JsonProcessingException {}

    /**
     * Performs a post request and inspects the response regarding its content, content type and HTTP response code.
     * The tests is successful when the test message has been successfully created.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(1)
    void successfulPostRequest() throws Exception {
        getMockMvc().perform(
                post(url)
                        .content(messageJSON)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(jsonPath("id").exists())
                    .andExpect(jsonPath("sender").exists())
                    .andExpect(jsonPath("receiver").exists())
                    .andExpect(jsonPath("message").value("Das ist ein Test"))
                    .andExpect(status().isOk())
                    .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its content type and HTTP response code.
     * The tests is successful when the HTTP status code is OK.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(2)
    void successfulGetAllRequest() throws Exception {
        getMockMvc().perform(
                get(url)
        )
                    .andDo(print())
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andDo(print());
    }

    /**
     * Performs a get request and inspects the response regarding its HTTP response code.
     * The tests is successful when the HTTP status code is Not Found.
     *
     * @throws Exception when HTTP request throws an error
     */
    @Test
    @Order(3)
    void notFoundError() throws Exception {
        getMockMvc().perform(
                get(url + "bad-id")
        )
                    .andExpect(status().isNotFound())
                    .andDo(print());
    }
}
