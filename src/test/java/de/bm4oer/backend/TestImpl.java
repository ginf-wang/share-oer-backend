package de.bm4oer.backend;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;

/**
 * Abstract class being used for tests.
 * It mocks our dummy user for tests that need user authentication and also starts the backend server.
 *
 * @author jlk@uni-bremen.de
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Transactional
@AutoConfigureMockMvc
@WithMockUser(username = BackendApplication.ApplicationSecurityConfig.username,
              password = BackendApplication.ApplicationSecurityConfig.password)
abstract class TestImpl {

    @Autowired
    private MockMvc mockMvc;

    /**
     * Build a doc snippet for every test, which includes .asciidoc documentation.
     *
     * @param webApplicationContext to apply to
     * @param restDocumentation     to be build
     */
    @BeforeEach
    public void setUp(final WebApplicationContext webApplicationContext,
                      final RestDocumentationContextProvider restDocumentation) {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                                 .apply(documentationConfiguration(restDocumentation).uris()
                                                                                     .withScheme("https")
                                                                                     .withHost("backend.bm4oer.de")
                                                                                     .withPort(443))
                                 .alwaysDo(document("{class_name}/{method_name}/{step}",
                                                    preprocessRequest(
                                                            prettyPrint()),
                                                    preprocessResponse(
                                                            prettyPrint())))
                                 .build();
    }

    MockMvc getMockMvc() {
        return mockMvc;
    }
}
